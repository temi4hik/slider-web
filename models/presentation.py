import logging

from google.appengine.api import taskqueue
from google.appengine.api.taskqueue.taskqueue import Task, Queue
import libs.cloudstorage as gcs
from google.appengine.ext import ndb
from libs.cloudstorage import errors as gcs_errors
import time
from pptx.enum.shapes import PP_PLACEHOLDER_TYPE
import content_types

from models.user import User
from methods.slide import SLIDE_TYPES, slide_requirements, TITLE_SLIDE, \
    TEXT_AND_PICTURE_SLIDE, TEXT_SLIDE, PICTURE_SLIDE, HERO_SLIDE, END_SLIDE
from hashlib import sha256

__author__ = 'Artem'

slide_descriptions = {
    TITLE_SLIDE: 'Title',
    TEXT_SLIDE: 'Text',
    HERO_SLIDE: 'Hero',
    TEXT_AND_PICTURE_SLIDE: 'Text and picture',
    PICTURE_SLIDE: 'Picture',
    END_SLIDE: 'End'
}

shapes_description = {
    PP_PLACEHOLDER_TYPE.TITLE: 'Title',
    PP_PLACEHOLDER_TYPE.SUBTITLE: 'Subtitle',
    PP_PLACEHOLDER_TYPE.BODY: 'Body',
    PP_PLACEHOLDER_TYPE.PICTURE: 'Picture'
}

image_types = ['cover_img', 'preview_title_img', 'preview_regular_img',
               'preview_hero_img', 'preview_image_img', 'preview_image_and_text_img', 'preview_end_img']


def set_file(file_path, file_data, content_type):
    with gcs.open(file_path, 'w', content_type=content_type) as f:
        f.write(file_data)
        f.close()
    return file_path


def delete_file(file_path):
    try:
        gcs.delete(file_path)
        return True
    except gcs_errors.NotFoundError:
        return False


def get_file(file_path):
    try:
        with gcs.open(file_path, 'r') as f:
            return f.read()
    except gcs_errors.NotFoundError:
        return None


def file_exists(file_path):
    try:
        with gcs.open(file_path, 'r'):
            return True
    except gcs_errors.NotFoundError:
        return False


class SliderFont(ndb.Model):
    name = ndb.StringProperty()
    gcs_file = ndb.StringProperty()

    def set_file(self, file_data):
        font_file_path = '/sliderpresentations.appspot.com/slider_fonts/{0}.otf'.format(self.key.id())
        content_type = 'font/opentype'

        with gcs.open(font_file_path, 'w', content_type=content_type) as f:
            f.write(file_data)

        self.gcs_file = font_file_path
        self.put()


class SlideProperties(ndb.Model):
    name = ndb.StringProperty()
    slide_type = ndb.IntegerProperty(choices=SLIDE_TYPES)
    preview_img = ndb.StringProperty()

    title_font = ndb.KeyProperty(kind=SliderFont)
    title_size = ndb.IntegerProperty(default=18)
    title_color = ndb.StringProperty(default='000000')

    text_font = ndb.KeyProperty(kind=SliderFont)
    text_size = ndb.IntegerProperty(default=18)
    text_color = ndb.StringProperty(default='000000')


class SliderBasicTemplate(ndb.Model):
    name = ndb.StringProperty()
    fonts = ndb.StringProperty(repeated=True)
    template_description = ndb.StringProperty(default="Slider template")
    gcs_file = ndb.StringProperty()
    is_wiki_template = ndb.BooleanProperty(default=False)
    owner = ndb.KeyProperty(kind=User)
    template_type = ndb.StringProperty(default='Slider_Basic_template')
    cover_img = ndb.StringProperty()


    def set_image(self, image_data, image_type):
        if image_type not in image_types:
            return False

        base_path = '/sliderpresentations.appspot.com/slider_basic_templates/{0}/'.format(self.key.id())
        path = base_path + image_type + '.png'

        with gcs.open(path, 'w', content_type='image/png') as f:
            f.write(image_data)

        self.__setattr__(image_type, path)
        self.put()
        return True

    def get_image(self, image_type):
        if image_type not in image_types:
            return None

        path = self.__getattribute__(image_type)
        return gcs.open(path, 'r').read()

    def delete_image(self, image_type):
        if image_type not in image_types:
            return False

        path = self.__getattribute__(image_type)

        return delete_file(path)

    def delete_template_file(self):
        return delete_file(self.template_file_path)

    @property
    def template_file_path(self):
        return '/sliderpresentations.appspot.com/slider_basic_templates/{0}/template.pptx'.format(self.key.id())

    @property
    def template_file(self):
        return get_file(self.template_file_path)

    @template_file.setter
    def template_file(self, file_data):
        set_file(self.template_file_path, file_data, content_types.PPTX)

    @property
    def fonts_str(self):
        return str(', '.join(self.fonts))

    def __dict__(self):
        return {
            'id': self.key.id(),
            'name': self.name,
            'description': self.template_description,
            'is_wiki': self.is_wiki_template
        }
        pass



class SliderTemplate(ndb.Model):
    name = ndb.StringProperty()
    fonts = ndb.StringProperty(repeated=True)
    template_description = ndb.StringProperty(default="Slider new template")
    gcs_file = ndb.StringProperty()
    is_wiki_template = ndb.BooleanProperty(default=True)

    # Symbol limits
    slide_symbols = ndb.IntegerProperty(default=600)
    heading_symbols = ndb.IntegerProperty(default=70)
    title_symbols = ndb.IntegerProperty(default=100)
    subtitle_symbols = ndb.IntegerProperty(default=300)
    hero_symbols = ndb.IntegerProperty(default=200)
    heading_phrase_symbols = ndb.IntegerProperty(default=20)
    image_with_text_symbols = ndb.IntegerProperty(default=300)

    cover_img = ndb.StringProperty()
    preview_title_img = ndb.StringProperty()
    preview_regular_img = ndb.StringProperty()
    preview_hero_img = ndb.StringProperty()
    preview_image_img = ndb.StringProperty()
    preview_image_and_text_img = ndb.StringProperty()
    preview_end_img = ndb.StringProperty()

    title_slide_props = ndb.StructuredProperty(SlideProperties)
    hero_slide_props = ndb.StructuredProperty(SlideProperties)
    picture_slide_props = ndb.StructuredProperty(SlideProperties)
    text_slide_props = ndb.StructuredProperty(SlideProperties)
    text_and_picture_slide_props = ndb.StructuredProperty(SlideProperties)
    end_slide_props = ndb.StructuredProperty(SlideProperties)

    def set_image(self, image_data, image_type):
        if image_type not in image_types:
            return False

        base_path = '/sliderpresentations.appspot.com/slider_templates/{0}/'.format(self.key.id())
        path = base_path + image_type + '.png'

        with gcs.open(path, 'w', content_type='image/png') as f:
            f.write(image_data)

        self.__setattr__(image_type, path)
        self.put()
        return True

    def get_image(self, image_type):
        if image_type not in image_types:
            return None

        path = self.__getattribute__(image_type)
        return gcs.open(path, 'r').read()

    def delete_image(self, image_type):
        if image_type not in image_types:
            return False

        path = self.__getattribute__(image_type)

        return delete_file(path)

    def delete_template_file(self):
        return delete_file(self.template_file_path)

    @property
    def template_file_path(self):
        return '/sliderpresentations.appspot.com/slider_templates/{0}/template.pptx'.format(self.key.id())

    @property
    def template_file(self):
        return get_file(self.template_file_path)

    @template_file.setter
    def template_file(self, file_data):
        set_file(self.template_file_path, file_data, content_types.PPTX)

    @property
    def fonts_str(self):
        return str(', '.join(self.fonts))

    def __dict__(self):
        return {
            'id': self.key.id(),
            'name': self.name,
            'description': self.template_description,
            'is_wiki': self.is_wiki_template
        }
        pass


class SliderSlide(ndb.Model):
    slide_type = ndb.IntegerProperty(choices=SLIDE_TYPES)
    title = ndb.StringProperty()  # Slide title. Can be None for Hero
    text = ndb.StringProperty()  # Slide text. None for End
    image_name = ndb.StringProperty()  # Name for image if slide has one

    def __dict__(self):
        return {
            'slide_type': self.slide_type,
            'title': self.title,
            'text': self.text,
            'image': self.image_name
        }


class SliderPresentation(ndb.Model):
    name = ndb.StringProperty(default="Name")
    template = ndb.KeyProperty()
    # template = ndb.KeyProperty(kind=SliderTemplate)
    gcs_file = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    owner = ndb.KeyProperty(kind=User)
    slides = ndb.StructuredProperty(SliderSlide, repeated=True)

    # region GCS path properties

    @property
    def base_path(self):
        if not self.owner:
            return '/sliderpresentations.appspot.com/slider_presentations/ios_devices/{0}'.format(self.key.id())
        else:
            return '/sliderpresentations.appspot.com/slider_presentations/users/{0}/{1}'.format(self.owner.id(),
                                                                                                self.key.id())

    @property
    def pptx_file_path(self):
        return '{0}/file.pptx'.format(self.base_path)

    @property
    def pdf_file_path(self):
        return '{0}/file.pdf'.format(self.base_path)

    @property
    def preview_base_path(self):
        return '{0}/preview'.format(self.base_path)

    @property
    def images_base_path(self):
        return '{0}/images'.format(self.base_path)

    def slide_preview_image_path(self, idx):
        return '{0}/preview/slide{1}.png'.format(self.base_path, idx)

    # endregion

    # region GCS files methods

    @property
    def has_pdf(self):
        return file_exists(self.pdf_file_path)

    @property
    def has_preview_images(self):
        return file_exists(self.slide_preview_image_path(0))

    @property
    def pptx_file(self):
        return get_file(self.pptx_file_path)

    @pptx_file.setter
    def pptx_file(self, file_data):
        set_file(self.pptx_file_path, file_data, content_types.PPTX)

    @pptx_file.deleter
    def pptx_file(self):
        delete_file(self.pptx_file_path)

    @property
    def pdf_file(self):
        return get_file(self.pdf_file_path)

    @pdf_file.setter
    def pdf_file(self, file_data):
        set_file(self.pdf_file_path, file_data, 'application/pdf')

    @pdf_file.deleter
    def pdf_file(self):
        delete_file(self.pdf_file_path)

    def set_slide_preview_image(self, idx, image_data):
        image_path = self.slide_preview_image_path(idx)
        return set_file(image_path, image_data, 'image/png')

    def get_slide_preview_image(self, idx):
        image_path = self.slide_preview_image_path(idx)
        return get_file(image_path)

    def delete_slide_preview_image(self, idx):
        image_path = self.slide_preview_image_path(idx)
        return delete_file(image_path)

    def set_image(self, image_name, image_data):
        image_path = '{0}/{1}.png'.format(self.images_base_path, image_name)
        return set_file(image_path, image_data, 'image/png')

    def get_image(self, image_name):
        image_path = '{0}/{1}.png'.format(self.images_base_path, image_name)
        return get_file(image_path)

    def delete_image(self, image_name):
        image_path = '{0}/{1}.png'.format(self.images_base_path, image_name)
        return delete_file(image_path)

    # endregion

    def add_slide(self, slide_type, title=None, text=None, image_name=None, image_data=None):
        slide = SliderSlide(slide_type=slide_type, title=title, text=text)

        if slide_type in (PICTURE_SLIDE, TEXT_AND_PICTURE_SLIDE):
            self.set_image(image_name, image_data)
            slide.image_name = image_name

        self.slides.append(slide)

    def __dict__(self):
        return {
            'id': self.key.id(),
            'name': self.name,
            'template_id': self.template.id(),
            'created': {
                'year': self.created.year,
                'month': self.created.month,
                'day': self.created.day,
                'hour': self.created.hour,
                'minute': self.created.minute,
                'second': self.created.second
            },
            'owner': self.owner.id() if self.owner else None,
            'slides': [slide.__dict__() for slide in self.slides],
            'has_pdf': self.has_pdf,
            'has_preview_images': self.has_preview_images
        }

    def set_destruction_timer(self, seconds=1800):
        taskqueue.add(url='/task/delete_presentation', method='POST', params={'presentation_id': self.key.id()},
                      countdown=seconds,
                      name=str(self.key.id()))


class SliderPresentationToken(ndb.Model):
    token = ndb.StringProperty()
    presentation = ndb.KeyProperty(kind=SliderPresentation)

    def generate_token(self, device_id):
        from math import exp

        if not self.presentation:
            return False

        pr = self.presentation.get()

        created_time = pr.created.ctime()
        now_time = str(round(time.time() * exp(7)))
        raw_str = '{0}-{1}-!{2}!{3}-{4}'.format(created_time,
                                                now_time,
                                                device_id,
                                                pr.key.id(),
                                                pr.template.id())

        self.token = sha256(raw_str).hexdigest()
        self.put()
        return True
