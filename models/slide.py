from pptx.enum.shapes import PP_PLACEHOLDER_TYPE
from PIL import Image
__author__ = 'Artem'


class AbstractSlide(object):
    def __init__(self, slide):
        self._slide = slide
        try:
            self._title = self.get_shape(PP_PLACEHOLDER_TYPE.TITLE)
        except IndexError:
            # if no title available - pass it
            pass

    def get_shape(self, shape_type):
        return [shape for shape in self._slide.shapes if shape.placeholder_format.type == shape_type][0]

    @property
    def title(self):
        return self._title.text

    @title.setter
    def title(self, value):
        self._title.text = value


class TitleSlide(AbstractSlide):
    def __init__(self, slide):
        super(TitleSlide, self).__init__(slide)

        self._subtitle = self.get_shape(PP_PLACEHOLDER_TYPE.SUBTITLE)

    @property
    def subtitle(self):
        return self._subtitle.text

    @subtitle.setter
    def subtitle(self, value):
        self._subtitle.text = value


class TextSlide(AbstractSlide):
    def __init__(self, slide):
        super(TextSlide, self).__init__(slide)

        self._text_frame = self.get_shape(PP_PLACEHOLDER_TYPE.BODY)

    def add_paragraph(self, text):
        self.text += '\n'
        self.text += text

    @property
    def text(self):
        return self._text_frame.text

    @text.setter
    def text(self, value):
        self._text_frame.text = value


class PictureSlide(AbstractSlide):
    def __init__(self, slide):
        super(PictureSlide, self).__init__(slide)
        self._picture = self.get_shape(PP_PLACEHOLDER_TYPE.PICTURE)

    def set_picture(self, img_path):
        image_file = open(img_path)
        self._picture.insert_picture(image_file)


class HeroSlide(AbstractSlide):
    def __init__(self, slide):
        super(HeroSlide, self).__init__(slide)
        self._text_frame = self.get_shape(PP_PLACEHOLDER_TYPE.BODY)

    @property
    def text(self):
        return self._text_frame.text

    @text.setter
    def text(self, value):
        self._text_frame.text = value


class TextAndPictureSlide(AbstractSlide):
    def __init__(self, slide):
        super(TextAndPictureSlide, self).__init__(slide)
        self._text_frame = self.get_shape(PP_PLACEHOLDER_TYPE.BODY)
        self._picture = self.get_shape(PP_PLACEHOLDER_TYPE.PICTURE)

    @property
    def text(self):
        return self._text_frame.text

    @text.setter
    def text(self, value):
        self._text_frame.text = value


class EndSlide(AbstractSlide):
    def __init__(self, slide):
        super(EndSlide, self).__init__(slide)
