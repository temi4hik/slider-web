import time

import webapp2_extras.appengine.auth.models
from google.appengine.ext import ndb
from webapp2_extras import security

HASH_LENGTH = 12


class User(webapp2_extras.appengine.auth.models.User, ndb.Model):
    is_admin = ndb.BooleanProperty(default=False)

    def __dict__(self):
        return {
            'id': self.get_id(),
            'email': self.email_address,
            'name': self.name,
            'last_name': self.last_name,
            'verified': self.verified
        }

    def set_password(self, raw_password):
        """
        Sets the password for the current user
        Args:
            raw_password: The raw password which will be hashed and stored
        """
        self.password = security.generate_password_hash(raw_password, length=HASH_LENGTH)

    @classmethod
    def get_by_auth_token(cls, user_id, token, subject='auth'):
        """
        Returns a user object based on a user ID and token
        Args:
            user_id: The user_id of the requesting user.
            token: The token string to be verified
        Returns:
            A tuple ''(User, timestamp)'', with a user object and
            the token timestamp, or ''(None, None)'' if both were not found
        """
        token_key = cls.token_model.get_key(user_id, subject, token)
        user_key = ndb.Key(cls, user_id)

        valid_token, user = ndb.get_multi([token_key, user_key])

        if valid_token and user:
            timestamp = int(time.mktime(valid_token.created.timetuple()))
            return user, timestamp

        return None, None


class ForgotPasswordToken(ndb.Model):
    token = ndb.StringProperty()
    user = ndb.KeyProperty(kind=User)

    def generate_token(self):
        from math import exp
        import time
        from hashlib import sha256
        now_time = str(round(time.time() * exp(7)))
        raw_str = '{0}-{1}-{2}'.format(now_time, self.key.id(), self.user.id())

        self.token = sha256(raw_str).hexdigest()
