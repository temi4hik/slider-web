from google.appengine.ext import ndb


class EmailInvite(ndb.Model):
    email = ndb.StringProperty()