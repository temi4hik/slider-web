
	function send_email() {
        console.log('here12');
        $.ajax({
            type: "POST",
            url: "/forgot",
            data: {
                email: $("#email_restore_input").val()
            },
            success: function (data) {
            	console.log(data);
            	if (data == 'success') {
            		console.log('here3');
            		$(".form_inp").css('display', 'none');
            		$("#response").addClass('reset_pass_h1');
            		$("#response").html('Link was sent on your email to restore password.');	
            		$("#response").removeClass('error');
            		$("#response").css('display', 'block');
            	} else if (data == 'failure') {
            		console.log(data);
            		$("#response").html('User with such email does not exists.');
            		$("#response").css('display', 'block');
            	} else {
            		$("#response").html('Some errror accured.');
            		$("#response").css('display', 'block');
            	}
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }

	function submit_pass() {
        console.log('here2');
        $.ajax({
            type: "POST",
            url: "/restore",
            data: {
                user_id: $("#user_id").val(),
                new_password: $("#new_password").val(),
                token: $("#token").val()
            },
            success: function (data) {
            	console.log(data);
            	if (data == 'success') {
            		console.log('here3');
            		$(".form_inp").css('display', 'none');
            		$("#response").text('Password was successfully changed.');
            		$("#response").removeClass('error');
            		$("#response").css('display', 'block');
            		$("#sign_in_link_2").css('display', 'block');
            	} else {
            		$("#response").text('Some error accured...');
            		$("#response").css('display', 'block');
            	}
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }


$(document).ready(function () {

	$.listen("click", "#sign_btn", function () {
        submit_pass();
    });

    $.listen("click", "#send_email_restore", function () {
        send_email();
    });

});