var profile_head_set_block = 'hidden';

$(document).ready(function () {

	$.listen("click", "#close_mnu", function () {
        $(".pres_menu_cont").css('left', '-250px');
    });

    $.listen("click", ".mnu_bl", function () {
        $(".pres_menu_cont").css('left', '0');
    });

	$.listen("click", ".open_left_mnu", function () {
        $(".pres_menu_cont").css('left', '0');
    });

    $.listen("click", "#home_mnu", function () {
        window.location = "http://slider.pw";
    });

	$.listen("click", "body", function () {
		$(".profile_head_set_block").css('display', 'none');
		profile_head_set_block = 'hidden';
	})

	$.listen("click", "#user_name", function () {
		console.log(profile_head_set_block);
		if (profile_head_set_block == 'hidden') {
			$(".profile_head_set_block").css('display', 'block');
			profile_head_set_block = 'visible';
		} else {
			$(".profile_head_set_block").css('display', 'none');
			profile_head_set_block = 'hidden';
		}
        
    });
	
});