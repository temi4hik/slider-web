function cropper() {
            /* 03.21. Cropperjs */
    var Cropper = window.Cropper;
    if (typeof Cropper !== "undefined") {
      function each(arr, callback) {
        var length = arr.length;
        var i;

        for (i = 0; i < length; i++) {
          callback.call(arr, arr[i], i, arr);
        }

        return arr;
      }
      var previews = document.querySelectorAll(".cropper-preview");
      var options = {
        aspectRatio: 4 / 3,
        preview: ".img-preview",
        ready: function() {
          var clone = this.cloneNode();

          clone.className = "";
          clone.style.cssText =
            "display: block;" +
            "width: 100%;" +
            "min-width: 0;" +
            "min-height: 0;" +
            "max-width: none;" +
            "max-height: none;";
          each(previews, function(elem) {
            elem.appendChild(clone.cloneNode());
          });
        },
        crop: function(e) {
          var data = e.detail;
          var cropper = this.cropper;
          var imageData = cropper.getImageData();
          var previewAspectRatio = data.width / data.height;

          each(previews, function(elem) {
            var previewImage = elem.getElementsByTagName("img").item(0);
            var previewWidth = elem.offsetWidth;
            var previewHeight = previewWidth / previewAspectRatio;
            var imageScaledRatio = data.width / previewWidth;
            elem.style.height = previewHeight + "px";
            if (previewImage) {
              previewImage.style.width =
                imageData.naturalWidth / imageScaledRatio + "px";
              previewImage.style.height =
                imageData.naturalHeight / imageScaledRatio + "px";
              previewImage.style.marginLeft = -data.x / imageScaledRatio + "px";
              previewImage.style.marginTop = -data.y / imageScaledRatio + "px";
            }
          });
        },
        zoom: function(e) {}
      };

      if ($("#inputImage").length > 0) {
        var inputImage = $("#inputImage")[0];
        var image = $("#cropperImage")[0];

        var cropper;
        inputImage.onchange = function() {
          var files = this.files;
          var file;

          if (files && files.length) {
            file = files[0];
            $("#cropperContainer").css("display", "block");

            if (/^image\/\w+/.test(file.type)) {
              uploadedImageType = file.type;
              uploadedImageName = file.name;

              image.src = uploadedImageURL = URL.createObjectURL(file);
              if (cropper) {
                cropper.destroy();
              }
              cropper = new Cropper(image, options);
              inputImage.value = null;
            } else {
              window.alert("Please choose an image file.");
            }
          }
        };
      }
    }
    }

$(document).ready(function () {


    cropper();



});