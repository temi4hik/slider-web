
var template_id_selected = "";
var selected_title = "";
var selected_sections = "";

var presentation_id = "";





// function download_file() {
// 	var link = document.getElementById("download_file").href;
// 	link = link.split('?token=');
// 	console.log(link);
    
//     $.ajax ({
//         type: "GET",
//         url: '/api/presentation/'+presentation_id+'/file',
//         data: {
//         	token: link[1]
//         },
//         contentType:"application/download",
//         success: function (data) {
//         	saveData(data,fileName);
//             // console.log(data);
//             // window.location = data;
//             // var a = document.createElement('a');
//             // var url = window.URL.createObjectURL(data);
//             // a.href = url;
//             // a.download = 'presentation.pptx';
//             // a.click();
//             // window.URL.revokeObjectURL(url);
//         },
//           error: function (xhr, ajaxOptions, thrownError) {
//             console.log(thrownError);
//         }
//     });
// }

function get_templates() {
    localStorage.setItem('presentation_text', $(".pres_content_box").val());
    $.ajax ({
            type: "POST",
            url: "/presentation/templates",
            success: function (data) {
                $(".content").html(data);
            },
              error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
}



function search_sections(topic) {
	$('#topic_sections').html('Searching sections <img src="https://www.seoclerk.com/pics/want41452-1eO47G1470623967.gif" width="20px" height="20px">'); 
	selected_title = topic;
	$.ajax({
        type: "GET",
        url: "/api/wiki/sections",
        data: {
        	title: topic,
        	language: $('#language').val()
        },
        contentType:"application/json",
        success: function (data) {
        	$('#topic_sections').html(''); 
            
            if (data['data'] != null) {
            	console.log(data['data']['sections']);
            	$('#topic_sections').append('<h2>Here are the sections we found for the selected topic! <br> Select the sections you need (could be more then one):</h2><div id="sections_container" class="col-md-12"></div>');
	            for (var i = 0; i <= data['data']['sections'].length - 1; i++) {
	            	$('#sections_container').append('<button id="select_topic" class="btn btn-outline-success my-2 my-sm-0 select_section_btn" type="submit">'+data['data']['sections'][i]+'</button>');
	            };
	            $('#sections_container').append('</div><div class="content con"><div>');
            } else {
            	$('#topic_sections').append('<div class="content con"><div>');
            }
            
            get_templates();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });
}


function search_topic() {
	console.log($('#language').val());
	$('#topic_sections').html('Searching topics <img src="https://www.seoclerk.com/pics/want41452-1eO47G1470623967.gif" width="20px" height="20px">'); 
	 $.ajax({
        type: "GET",
        url: "/api/wiki/search",
        data: {
        	query: $('#search_input').val(),
        	language: $('#language').val()
        },
        contentType:"application/json",
        success: function (data) {
        	$('#topic_sections').html(''); 

        	if (data['data']['results'].length == 0) {
        		$('#topic_sections').append('<h2>No topics found, try another keyword!</h2><div id="topics_container" class="col-md-12"></div>');
        	} else {
        		console.log(data['data']['results']);
	            $('#topic_sections').append('<h2>Here are the topics we found! <br> Select one to procced:</h2><div id="topics_container" class="col-md-12"></div>');


	            for (var i = 0; i <= data['data']['results'].length - 1; i++) {
	            	var row = '';
	            	if ((i+1) % 4 == 0) {
	            		row = '</div><div class="row">';
	            	}
	            	$('#topics_container').append(row+'<div class="col-md-3"><button id="select_topic" class="btn btn-outline-success my-2 my-sm-0 select_topic_btn col-md-3" type="submit">'+data['data']['results'][i]+'</button></div>');
	            };
        	}

            
            

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });
}

function generate_presentation() {
	var all_sel_sections = $(".selected").map(function() {
	    return this.innerHTML;
	}).get();
	console.log(all_sel_sections);
	console.log(template_id_selected);
	selected_sections = all_sel_sections.join('**');
	console.log(localStorage.getItem('device_id'));
	var device_id = localStorage.getItem('device_id');
	if (device_id == null) {
		var random = Math.floor(Math.random() * 9999999999999) + 1;
		localStorage.setItem('device_id', random)
		console.log(localStorage.getItem('device_id'));
	};

	console.log(selected_title);
	console.log(selected_sections);
	console.log(localStorage.getItem('device_id'));
	$('#topic_sections').html('Generating presentation <img src="https://www.seoclerk.com/pics/want41452-1eO47G1470623967.gif" width="20px" height="20px">'); 

	$.ajax({
        type: "POST",
        url: "/api/wiki/make",
        data: {
        	title: selected_title,
        	template_id: template_id_selected,
        	device_id: localStorage.getItem('device_id'),
        	sections: selected_sections,
        	language: $('#language').val()
        },
        success: function (data) {
        	console.log(data);

        	presentation_id = data['data']['id'];
        	
        	$('#topic_sections').html('<a id="download_file" href="'+data['data']['url']+'">Download presentation!</a>'); 

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });

}


$(document).ready(function () {

	$.listen("click", "#search_topic", function () {
		$([document.documentElement, document.body]).animate({
		        scrollTop: $("#search_input").offset().top
		    }, 1000);
		
		search_topic();
	});

	$.listen("click", ".select_topic_btn", function () {
		$([document.documentElement, document.body]).animate({
		        scrollTop: $("#search_input").offset().top
		    }, 1000);
		
		search_sections($(this).text());
	});

	$.listen("click", ".select_section_btn", function () {
		$(this).toggleClass('selected');
		// search_sections($(this).text()); submit_template
	});

	$.listen("click", ".chosen_block", function () {
		$([document.documentElement, document.body]).animate({
		        scrollTop: $("#search_input").offset().top
		    }, 1000);

		generate_presentation();
	});


	$.listen("click", ".select_template", function () {
        var template_id = this.id;
        var template_id_selected_list = template_id.split('/');
        if (template_id_selected_list.length == 2) {
            template_id_selected = template_id_selected_list[1];
        } else {
            template_id_selected = template_id_selected_list[0];
        }
        console.log(template_id_selected);
        $(".template_img_cont").removeClass('active');
        $("#"+template_id_selected+".template_img_cont").addClass('active');
    });


});

