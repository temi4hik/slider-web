
var ajax = '';


$.fn.imagesLoaded = function () {

    // get all the images (excluding those with no src attribute)
    var $imgs = this.find('img[src!=""]');
    // if there's no images, just return an already resolved promise
    if (!$imgs.length) {return $.Deferred().resolve().promise();}

    // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
    var dfds = [];  
    $imgs.each(function(){

        var dfd = $.Deferred();
        dfds.push(dfd);
        var img = new Image();
        img.onload = function(){dfd.resolve();}
        img.onerror = function(){dfd.resolve();}
        img.src = this.src;

    });

    // return a master promise object which will resolve when all the deferred objects have resolved
    // IE - when all the images are loaded
    return $.when.apply($,dfds);

}

function new_presentation() {
	var header = '<div class="row"><div class="col-12 list"><div class="mb-2"><h1>New presentation</h1><div class="float-sm-right"> <button type="button" class="btn btn-lg btn-outline-primary dropdown-toggle dropdown-toggle-split top-right-button top-right-button-single" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ACTIONS </button><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(955px, 42px, 0px); top: 0px; left: 0px; will-change: transform;"> <a class="dropdown-item" href="#" id="generate_presentation">Generate presentation</a> <a class="dropdown-item" href="#">Another action</a></div></div></div><div class="separator mb-5"></div><div  id="load_gif"  style="width:100%;"><img src="/static/images/loader.gif" style=" width: 200px; height: 110px; display: block; margin: auto; "></div></div><div class="tab-content"></div>';
	$("#dashboard_cont").html(header); 
	$(".tab-content").css('opacity','0'); 
    ajax = $.ajax({
            type: "GET",
            url: "/presentation/dash_new_pres",
            success: function (data) {
                $(".tab-content").append(data).imagesLoaded().then(function(){
            		// do stuff after images are loaded here my_presentation_cont
            		$("#load_gif").remove();
            		$(".tab-content").css('opacity','1'); 
        		});
                // get_presentation();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }

function my_presentations() {
	
	var header = '<div class="row"><div class="col-12"><div class="mb-3"><h1>My presentations</h1><div class="float-sm-right text-zero"></div></div><div class="mb-2"> <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions" role="button" aria-expanded="true" aria-controls="displayOptions"> Display Options <i class="simple-icon-arrow-down align-middle"></i> </a></div><div class="separator mb-5"></div><div id="load_gif"  style="width:100%;"><img src="/static/images/loader.gif" style=" width: 200px; height: 110px; display: block; margin: auto; "><div></div></div><div class="row list disable-text-selection my_presentation_cont" data-check-all="checkAll"></div>';
    $("#dashboard_cont").html(header);   
    $(".my_presentation_cont").css('opacity','0'); 
    ajax = $.ajax({
            type: "GET",
            url: "/presentation/my_presentations",
            success: function (data) {
                $(".my_presentation_cont").append(data).imagesLoaded().then(function(){
            		// do stuff after images are loaded here my_presentation_cont
            		$("#load_gif").remove();
            		$(".my_presentation_cont").css('opacity','1');
        		});
                // get_presentation();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }


function my_templates() {
    
    var header = '<div class="row"><div class="col-12"><div class="mb-3"><h1>My templates</h1><div class="float-sm-right text-zero"></div></div><div class="mb-2"> <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions" role="button" aria-expanded="true" aria-controls="displayOptions"> Display Options <i class="simple-icon-arrow-down align-middle"></i> </a></div><div class="separator mb-5"></div><div id="load_gif"  style="width:100%;"><img src="/static/images/loader.gif" style=" width: 200px; height: 110px; display: block; margin: auto; "><div></div></div><div class="row list disable-text-selection my_presentation_cont" data-check-all="checkAll"></div>';
    $("#dashboard_cont").html(header);   
    $(".my_presentation_cont").css('opacity','0'); 
    ajax = $.ajax({
            type: "GET",
            url: "/dashboard/my_templates",
            success: function (data) {
                $(".my_presentation_cont").append(data).imagesLoaded().then(function(){
                    // do stuff after images are loaded here my_presentation_cont
                    $("#load_gif").remove();
                    $(".my_presentation_cont").css('opacity','1');
                });
                // get_presentation();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }


function add_template_cont() {
	
	var header = '<div class="row"><div class="col-12 list"><div class="mb-2"><h1>Add template</h1><div class="float-sm-right"> <button type="button" class="btn btn-lg btn-outline-primary dropdown-toggle dropdown-toggle-split top-right-button top-right-button-single" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ACTIONS </button><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(955px, 42px, 0px); top: 0px; left: 0px; will-change: transform;"> <a id="upload_new_template" class="dropdown-item" href="#">Upload</a> </div></div></div><div class="separator mb-5"></div><div  id="load_gif"  style="width:100%;"><img src="/static/images/loader.gif" style=" width: 200px; height: 110px; display: block; margin: auto; "></div></div><div class="add_template"></div>';
    $("#dashboard_cont").html(header);   
    $(".add_template").css('opacity','0'); 
    ajax = $.ajax({
            type: "GET",
            url: "/admin/templates/add_in_dashboard",
            success: function (data) {
                $(".add_template").append(data).imagesLoaded().then(function(){
            		// do stuff after images are loaded here my_presentation_cont
            		$("#load_gif").remove();
            		$(".add_template").css('opacity','1');
        		});
                // get_presentation();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }


function generate_presentation() {
    console.log('started');
    var slides = [];

    $('.slide_content_row').each(function(i, obj) {
        slide_placeholders = [];
        $('.text_input_'+i).each(function(y, container) {
            slide_placeholders.push($(container).val());
        });
        var slide_info = {index:i, slide_placeholders: slide_placeholders}
        slides.push(slide_info);
    });
    slides.splice(0,1);



    console.log(slides);
    
    ajax = $.ajax({
        type: "POST",
        url: "/presentation/dash_new_pres",
        data: {
            slides_cont: slides,
            template_id: $('.slide_list').attr('id')
        },
        success: function (data) {

            console.log(data);


        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError);
        }
    });
}



$(document).ready(function () {

	my_presentations();

    $.listen("click", "#create_presentation", function (event) {
        event.preventDefault();
        ajax.abort();
        new_presentation();
    });

    $.listen("click", "#add_template", function (event) {
        event.preventDefault();
        ajax.abort();
        add_template_cont();
    });

    $.listen("click", ".sub_menu li a", function () {
    	if (!$(this).parent().hasClass('active')) {
			$('.sub_menu li.active').removeClass('active');
    		$(this).parent().addClass('active');
    	}
        
    });

    
    $.listen("click", "#my_presentations", function (event) {
        event.preventDefault();
        ajax.abort();
        my_presentations();
    });

    $.listen("click", "#my_templates", function (event) {
        event.preventDefault();
        ajax.abort();
        my_templates();
    });

    $.listen("click", "#generate_presentation", function (event) {
        event.preventDefault();
        generate_presentation();
    });

    $.listen("click", ".slide_card", function (event) {

        event.preventDefault();
        var id_0 = $(this).attr('id');
        id = id_0.split('slide_');
        id = id[1];

        $('.slide_card').removeClass('active');
        $('#'+id_0).addClass('active');

        $('.slide_content_row').prop('hidden','true');

        // var $listSort = $('#slide_content_'+id);
        $('#slide_content_'+id).removeProp('hidden');
        $('#slide_content_'+id).removeAttr('hidden');

    });




});