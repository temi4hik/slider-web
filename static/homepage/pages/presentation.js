/**
 * Created by Gregory on 22.05.2016.
 */

var template_id_selected = "";
	function get_textbox() {
        $.ajax ({
                type: "POST",
                url: "/presentation/content",
                success: function (data) {
                    $(".content").html(data);
                    $(".pres_content_box").val(localStorage.getItem('presentation_text'));
                },
                  error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
	}

	function get_templates() {
        localStorage.setItem('presentation_text', $(".pres_content_box").val());
        $.ajax ({
                type: "POST",
                url: "/presentation/templates",
                success: function (data) {
                    $(".content").html(data);
                },
                  error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
	}

    function load_generating_page() {
        $.ajax({
            type: "POST",
            url: "/presentation/generation",
            success: function (data) {
                $(".content").html(data);
                get_presentation();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }

    function get_presentation() {
        $.ajax({
            type: "POST",
            url: "/presentation/get",

            data: {
                text: localStorage.getItem('presentation_text'),
                template_id: template_id_selected
            },
            success: function (data) {
                load_save_page(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }

    function load_save_page(id) {
        $.ajax({
            type: "POST",
            url: "/presentation/save",
            data: {
                pres_id: id
            },
            success: function (data) {
                $(".content").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }

$(document).ready(function () {


    $.listen("click", ".select_template", function () {
        var template_id = this.id;
        var template_id_selected_list = template_id.split('/');
        if (template_id_selected_list.length == 2) {
            template_id_selected = template_id_selected_list[1];
        } else {
            template_id_selected = template_id_selected_list[0];
        }
        console.log(template_id_selected);
        $(".template_img_cont").removeClass('active');
        $("#"+template_id_selected+".template_img_cont").addClass('active');
    });

    $.listen("click", "#submit_template", function () {
        load_generating_page();
        console.log('generating');
    });

    $.listen("click", "#submit_text", function () {
        get_templates();
    });

    $.listen("click", ".start_btn", function () {
        get_textbox();
    });


    $.listen("click", ".start_btn_img", function () {
        get_textbox();
    });

    

});