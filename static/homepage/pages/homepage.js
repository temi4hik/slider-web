var emailFormResponse = $('#email_form_response');
var emailSliderForm = $('#email_slider_form');


function sendEmail() {
    var email = emailSliderForm.val();
    $.ajax({
        type: "POST",
        data: {
            email_slider_form: email
        },
        success: function (data) {

            if (data == 'success') {
                emailFormResponse.css("color", "green");
                emailFormResponse.text('Thank you for choosing Slider! You will get the link on your email soon!');
            } else {
                emailFormResponse.css("color", "red");
                emailFormResponse.text('Something went wrong :( Please, contact support@slider.pw');
            }
            emailSliderForm.val('');
        }
    });
}

function checkEmail(email) {
    if (email != '') {
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if (pattern.test(email)) {
            sendEmail(email);
            $('#email_form_response').text('');
        } else {
            emailFormResponse.css("color", "red");
            emailFormResponse.text('Incorrect email');
        }
    } else {
        emailFormResponse.css("color", "red");
        emailFormResponse.text('Please, enter your email!');
    }
}

$(document).ready(function () {
    $.listen("click", "#submit_slider_form", function () {
        var email = $("#email_slider_form").val();
        checkEmail(email);
    });
});