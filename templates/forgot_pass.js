

	function submit_pass() {
        $.ajax({
            type: "POST",
            url: "/restore",
            data: {
                user_id: $("#user_id").val(),
                new_password: $("#new_password").val()
            },
            success: function (data) {
            	if (data['status'] == 'success') {
            		$("#response").html(data['message']);
            	} else {
            		$("#response").html(data['error']);
            	}
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    }


$(document).ready(function () {

	$.listen("click", "#sign_btn", function () {
        submit_pass();
    });

});