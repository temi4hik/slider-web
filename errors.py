# coding=utf-8
PresentationNotAvailableError = 'presentation_not_available_error'
PresentationAccessError = 'presentation_access_error'

InvalidRequestError = 'invalid_request_error'

AlreadyLoggedInAuthError = 'already_logged_in_auth_error'
InvalidEmailOrPasswordAuthError = 'email_password_auth_error'
LogoutAuthError = 'logout_auth_error'
AlreadyRegisteredAuthError = 'already_registered_auth_error'
AccessDeniedAuthError = 'access_denied_auth_error'
UserDoesNotExistsError = 'user_does_not_exists_error'

ImageNotAvailableError = 'image_not_available_error'
PDFNotAvailableError = 'pdf_not_available_error'
InvalidSlideImageError = 'invalid_slide_image_error'

WikiInvalidPageError = 'wiki_invalid_page_error'
WikiNoTitleError = 'wiki_no_title_error'

error_description = {
    'en': {
        InvalidRequestError: u'Invalid request.',
        PresentationNotAvailableError: u'This presentation is no longer available or never existed.',
        PresentationAccessError: u'You have no access to this presentation.',
        AlreadyLoggedInAuthError: u'You are already logged in.',
        InvalidEmailOrPasswordAuthError: u'Invalid email or password.',
        LogoutAuthError: u'You are not logged in to log out.',
        AlreadyRegisteredAuthError: u'User is already registered.',
        AccessDeniedAuthError: u'You are not authorized to see this page.',
        UserDoesNotExistsError: u'User with such email does not exists.',
        ImageNotAvailableError: u'Preview image is not available',
        PDFNotAvailableError: u'PDF file is not available.',
        InvalidSlideImageError: u'Invalid slide index',
        WikiInvalidPageError: u'Page with such title does not exist',
        WikiNoTitleError: u'No title given'
    },
    'ru': {
        InvalidRequestError: u'Неверный запрос.',
        PresentationNotAvailableError: u'Презентация более недоступна или никогда не существовала.',
        PresentationAccessError: u'Ошибка доступа.',
        AlreadyLoggedInAuthError: u'Вы уже авторизованы.',
        InvalidEmailOrPasswordAuthError: u'Неверный email или пароль.',
        LogoutAuthError: u'Вы не авторизованы для выхода.',
        AlreadyRegisteredAuthError: u'Пользователь уже зарегистрирован.',
        AccessDeniedAuthError: u'Требуется авторизация.',
        UserDoesNotExistsError: u'Пользователя с таким email не существует.',
        ImageNotAvailableError: u'Изображение с предпросмотром недоступно.',
        PDFNotAvailableError: u'PDF файл недоступен.',
        InvalidSlideImageError: u'Неверный номер слайда.',
        WikiInvalidPageError: u'Такой страницы не существует',
        WikiNoTitleError: u'Отсутствует название страницы'
    }
}
