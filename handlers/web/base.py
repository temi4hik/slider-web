import logging

import webapp2
from webapp2_extras import jinja2, auth, sessions


def user_required(handler):
    """
    Decorator that checks if there's a user associated with the current session.
    Will also fail if there's no session present.
    """
    def check_login(self, *args, **kwargs):
        if not self.auth.get_user_by_session():
            self.redirect_to('sign_in', _abort=True)
        else:
            return handler(self, *args, **kwargs)

    return check_login


def admin_required(handler):
    def check_admin(self, *args, **kwargs):
        u = self.user
        logging.debug(u)
        if u and u.is_admin:
            return handler(self, *args, **kwargs)
        else:
            body = {
                'error': 'You have no rights to see this page'
            }
            self.redirect_to('sign_in', _abort=True, _body=body)

    return check_admin


class BaseWebHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def auth(self):
        """Shortcut to access the auth instance as a property"""
        return auth.get_auth()

    @webapp2.cached_property
    def user_info(self):
        """
        Shortcut to access a subset of the user attributes that are stored
        in the session
        The list of attributes to store in the session is specified in
        config['webapp2_extras.auth']['user_attributes']
        Returns:
            A dictionary with most user information
        """
        return self.auth.get_user_by_session()

    @webapp2.cached_property
    def user(self):
        """
        Shortcut to access the current logged in user.

        Returns:
            The instance of the user model associated to the logged in user.
        """

        u = self.user_info
        return self.user_model.get_by_id(u['user_id']) if u else None

    @webapp2.cached_property
    def user_model(self):
        """
        Returns:
            Implementation of the user model.
        """
        return self.auth.store.user_model

    @webapp2.cached_property
    def session(self):
        """Shortcut to access the current session"""
        return self.session_store.get_session(backend="datastore")

    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def render_template(self, view_filename, **values):
        user = self.user_info
        values.update(user=user)
        rendered = self.jinja2.render_template(view_filename, **values)
        self.response.write(rendered)

    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)
