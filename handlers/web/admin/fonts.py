# coding=utf-8
from models.presentation import SliderFont
from handlers.web.base import BaseWebHandler, admin_required


# SLIDE_TYPES = (TITLE_SLIDE, TEXT_SLIDE, HERO_SLIDE, TEXT_AND_PICTURE_SLIDE, PICTURE_SLIDE, END_SLIDE)
class FontListHandler(BaseWebHandler):
    @admin_required
    def get(self):
        fonts = SliderFont.query().fetch()
        self.render_template('/admin/fonts/list.html', fonts=fonts)


class FontAddHandler(BaseWebHandler):
    @admin_required
    def get(self):

        self.render_template('/admin/fonts/add.html')

    @admin_required
    def post(self):
        slider_font = SliderFont()
        slider_font.put()

        name = self.request.get('name')
        font_file = self.request.get('font_file')

        slider_font.set_file(font_file)
        slider_font.name = name

        slider_font.put()

        self.redirect_to('fonts_list')
