from handlers.web.base import BaseWebHandler, admin_required
from methods.slide import TITLE_SLIDE, TEXT_SLIDE, HERO_SLIDE, TEXT_AND_PICTURE_SLIDE, PICTURE_SLIDE, END_SLIDE
from models.presentation import SliderTemplate, image_types, SliderFont, SlideProperties


class TemplateEditSlides(BaseWebHandler):
    @admin_required
    def get(self):
        template_id = self.request.get_range('template_id')
        template = SliderTemplate.get_by_id(template_id)
        fonts = SliderFont.query().fetch()
        self.render_template('/admin/templates/slides.html', template=template, fonts=fonts)

    @admin_required
    def post(self):
        template_id = self.request.get_range('template_id')
        template = SliderTemplate.get_by_id(template_id)

        # title slide values
        title_title_font_id = self.request.get_range('title_title_font')
        title_title_size = self.request.get_range('title_title_size')
        title_title_color = self.request.get('title_title_color')

        title_subtitle_font_id = self.request.get_range('title_subtitle_font')
        title_subtitle_size = self.request.get_range('title_subtitle_size')
        title_subtitle_color = self.request.get('title_subtitle_color')

        # text slide values
        text_title_font_id = self.request.get_range('text_title_font')
        text_title_size = self.request.get_range('text_title_size')
        text_title_color = self.request.get('text_title_color')

        text_text_font_id = self.request.get_range('text_text_font')
        text_text_size = self.request.get_range('text_text_size')
        text_text_color = self.request.get('text_text_color')

        # hero slide values
        hero_text_font_id = self.request.get_range('hero_text_font')
        hero_text_size = self.request.get_range('hero_text_size')
        hero_text_color = self.request.get('hero_text_color')

        # text and picture slide values
        text_and_picture_title_font_id = self.request.get_range('text_and_picture_title_font')
        text_and_picture_title_size = self.request.get_range('text_and_picture_title_size')
        text_and_picture_title_color = self.request.get('text_and_picture_title_color')

        text_and_picture_text_font_id = self.request.get_range('text_and_picture_text_font')
        text_and_picture_text_size = self.request.get_range('text_and_picture_text_size')
        text_and_picture_text_color = self.request.get('text_and_picture_text_color')

        # Picture slide values
        picture_title_font_id = self.request.get_range('picture_title_font')
        picture_title_size = self.request.get_range('picture_title_size')
        picture_title_color = self.request.get('picture_title_color')

        # End slide values
        end_title_font_id = self.request.get_range('end_title_font')
        end_title_size = self.request.get_range('picture_title_size')
        end_title_color = self.request.get('picture_title_color')

        title_title_font = SliderFont.get_by_id(title_title_font_id)
        title_subtitle_font = SliderFont.get_by_id(title_subtitle_font_id)
        template.title_slide_props = SlideProperties(name='Title', slide_type=TITLE_SLIDE,
                                                     title_font=title_title_font.key,
                                                     title_size=title_title_size, title_color=title_title_color,
                                                     text_font=title_subtitle_font.key,
                                                     text_size=title_subtitle_size, text_color=title_subtitle_color)

        text_title_font = SliderFont.get_by_id(text_title_font_id)
        text_text_font = SliderFont.get_by_id(text_text_font_id)

        template.text_slide_props = SlideProperties(name='Text', slide_type=TEXT_SLIDE,
                                                    title_font=text_title_font.key,
                                                    title_size=text_title_size, title_color=text_title_color,
                                                    text_font=text_text_font.key,
                                                    text_size=text_text_size, text_color=text_text_color)

        hero_text_font = SliderFont.get_by_id(hero_text_font_id)
        template.hero_slide_props = SlideProperties(name='Hero', slide_type=HERO_SLIDE,
                                                    text_font=hero_text_font.key,
                                                    text_size=hero_text_size, text_color=hero_text_color)

        text_and_picture_title_font = SliderFont.get_by_id(text_and_picture_title_font_id)
        text_and_picture_text_font = SliderFont.get_by_id(text_and_picture_text_font_id)

        template.text_and_picture_slide_props = SlideProperties(name='Text and picture',
                                                                slide_type=TEXT_AND_PICTURE_SLIDE,
                                                                title_font=text_and_picture_title_font.key,
                                                                title_size=text_and_picture_title_size,
                                                                title_color=text_and_picture_title_color,
                                                                text_font=text_and_picture_text_font.key,
                                                                text_size=text_and_picture_text_size,
                                                                text_color=text_and_picture_text_color)

        picture_title_font = SliderFont.get_by_id(picture_title_font_id)

        template.picture_slide_props = SlideProperties(name='Picture',
                                                       slide_type=PICTURE_SLIDE,
                                                       title_font=picture_title_font.key,
                                                       title_size=picture_title_size,
                                                       title_color=picture_title_color)

        end_title_font = SliderFont.get_by_id(end_title_font_id)

        template.end_slide_props = SlideProperties(name='End',
                                                   slide_type=END_SLIDE,
                                                   title_font=end_title_font.key,
                                                   title_size=end_title_size,
                                                   title_color=end_title_color)

        template.put()

        self.redirect_to('templates_list')


class TemplatesListHandler(BaseWebHandler):
    @admin_required
    def get(self):
        templates = SliderTemplate.query().fetch()
        self.render_template('/admin/templates/list.html', templates=templates)

    @admin_required
    def post(self):
        template_id = self.request.get_range('template_id')
        slider_template = SliderTemplate.get_by_id(template_id)

        # slider_template.delete_image
        for t in image_types:
            slider_template.delete_image(t)

        slider_template.delete_template_file()
        slider_template.key.delete()

        self.redirect_to('templates_list')


class TemplateAddHandler(BaseWebHandler):
    @admin_required
    def get(self):
        self.render_template('/admin/templates/add.html')

    @admin_required
    def post(self):
        slider_template = SliderTemplate()
        slider_template.put()

        name = self.request.get('name')
        template_file = self.request.get('template_file')
        slide_symbols = self.request.get_range('slide_symbols')
        heading_symbols = self.request.get_range('heading_symbols')
        title_symbols = self.request.get_range('title_symbols')
        subtitle_symbols = self.request.get_range('subtitle_symbols')
        hero_symbols = self.request.get_range('hero_symbols')
        heading_phrase_symbols = self.request.get_range('heading_phrase_symbols')
        image_with_text_symbols = self.request.get_range('image_with_text_symbols')

        template_description = self.request.get('template_description')
        fonts_str = self.request.get('fonts')
        fonts = fonts_str.replace(' ', '').split(',')

        for t in image_types:
            img = self.request.get(t)
            if img:
                slider_template.set_image(img, t)

        slider_template.template_file = template_file
        slider_template.name = name
        slider_template.template_description = template_description

        slider_template.slide_symbols = slide_symbols
        slider_template.heading_symbols = heading_symbols
        slider_template.title_symbols = title_symbols
        slider_template.subtitle_symbols = subtitle_symbols
        slider_template.hero_symbols = hero_symbols
        slider_template.heading_phrase_symbols = heading_phrase_symbols
        slider_template.fonts = fonts
        slider_template.image_with_text_symbols = image_with_text_symbols

        slider_template.put()

        self.redirect_to('templates_list')


class TemplateEditHandler(BaseWebHandler):

    @admin_required
    def get(self):
        pass
        template_id = self.request.get_range('template_id')
        template = SliderTemplate.get_by_id(template_id)

        self.render_template('/admin/templates/edit.html', template=template)

    @admin_required
    def post(self):
        template_id = self.request.get_range('template_id')
        slider_template = SliderTemplate.get_by_id(template_id)
        name = self.request.get('name')

        template_file = self.request.get('template_file')
        template_file_changed = self.request.get('template_file_changed') is not ''

        if template_file_changed:
            slider_template.template_file = template_file

        for t in image_types:
            t_c = t + '_changed'
            img_changed = self.request.get(t_c) is not ''
            img = self.request.get(t)
            if img_changed and img:
                slider_template.set_image(img, t)

        slide_symbols = self.request.get_range('slide_symbols')
        heading_symbols = self.request.get_range('heading_symbols')
        title_symbols = self.request.get_range('title_symbols')
        subtitle_symbols = self.request.get_range('subtitle_symbols')
        hero_symbols = self.request.get_range('hero_symbols')
        heading_phrase_symbols = self.request.get_range('heading_phrase_symbols')
        image_with_text_symbols = self.request.get_range('image_with_text_symbols')
        template_description = self.request.get('template_description')

        fonts_str = self.request.get('fonts')
        fonts = fonts_str.replace(' ', '').split(',')

        slider_template.name = name
        slider_template.template_description = template_description

        slider_template.slide_symbols = slide_symbols
        slider_template.heading_symbols = heading_symbols
        slider_template.title_symbols = title_symbols
        slider_template.subtitle_symbols = subtitle_symbols
        slider_template.hero_symbols = hero_symbols
        slider_template.heading_phrase_symbols = heading_phrase_symbols
        slider_template.image_with_text_symbols = image_with_text_symbols

        slider_template.fonts = fonts

        slider_template.put()
        self.redirect_to('templates_list')
