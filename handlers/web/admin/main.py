from handlers.web.base import BaseWebHandler, admin_required


class MainAdminHandler(BaseWebHandler):
    @admin_required
    def get(self):
        self.render_template('/admin/main.html')
