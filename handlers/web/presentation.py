from datetime import datetime
from jinja2 import Template
from pptx import Presentation

import cloudstorage as gcs
from io import BytesIO

from handlers.web.base import BaseWebHandler, user_required
from methods.slide import make_presentation, TITLE_SLIDE, TEXT_SLIDE, HERO_SLIDE, TEXT_AND_PICTURE_SLIDE, PICTURE_SLIDE, \
    END_SLIDE
from models.presentation import *


# Test comment

class PresentationPageHandler(BaseWebHandler):
    def get(self):
        self.render_template('presentation/main.html')

    def post(self):
        pass


class CreatePresentationWindowHandler(BaseWebHandler):
    def post(self):
        self.render_template('presentation/content/create_presentation.html')


class PresentationContentWindowHandler(BaseWebHandler):
    def post(self):
        self.render_template('presentation/content/insert_text.html')


# def __delete_slide(self, index):
#         presentation = self.prs
#         xml_slides = presentation.slides._sldIdLst
#         slides = list(xml_slides)
#         xml_slides.remove(slides[index])    

# for i in range(0,6,1):
#         delete_slides(prs, 0)

# def delete_slides(presentation, index):
#         xml_slides = presentation.slides._sldIdLst  
#         slides = list(xml_slides)
#         xml_slides.remove(slides[index])  

class PresentationNewWebHandler(BaseWebHandler):
    @user_required
    def get(self):
        prs = ''
        template_id = 5761438714953728
        template = SliderBasicTemplate.get_by_id(int(template_id))
        with gcs.open(template.template_file_path) as template_file:
            prs = Presentation(template_file)

        placeholders = []
        slide_index = 0

        for slide in prs.slides:
            slide_index = slide_index+1
            slide_container = []

            for shape in slide.shapes:
                if not shape.has_text_frame:
                    continue
                for paragraph in shape.text_frame.paragraphs:
                    p_text = paragraph.text
                    if '{{' in p_text:
                        p_text_0 = p_text.split('{{')
                        iterations = int(len(p_text_0)-1) 
                        for x in range(0,iterations):
                            b = x+1
                            p_text_1 = p_text_0[b].split('}}')
                            slide_container.append(p_text_1[0])

            slide_row = {'index': slide_index, 'slide_container': slide_container, 'template_id':template_id}
            placeholders.append(slide_row)


        self.render_template('presentation/content/dash_create_presentation.html', placeholders=placeholders)


    def post(self):
        prs = ''
        template_id = 5761438714953728
        slides_cont = self.request.get('slides')


        template = SliderBasicTemplate.get_by_id(int(template_id))
        with gcs.open(template.template_file_path) as template_file:
            prs = Presentation(template_file)


        slide_template_count = 1
        for slide in prs.slides:
            
            for shape in slide.shapes:
                if not shape.has_text_frame:
                    continue
                for paragraph in shape.text_frame.paragraphs:
                    p_text = paragraph.text

                    slide_edited_count = 0
                    for slide_cont in slides_cont:
                        if slide_cont['index'] == slide_template_count:
                            slide_container_input = slide_cont['slide_placeholders']
                            p_text_new = ''

                            if '{{' in p_text:
                                p_text_0 = p_text.split('{{')
                                iterations = int(len(p_text_0)-1) 
                                for x in range(0,iterations):
                                    b = x+1
                                    p_text_1 = p_text_0[b].split('}}')

                                    if x==0:
                                        if '}}' in p_text_0[b-1]:
                                            p_text_new = p_text_new + p_text_0[b-1].split('}}')[1]
                                        else:
                                            p_text_new = p_text_new + p_text_0[b-1]

                                    p_text_new = p_text_new + str(slide_container_input[x])
                                    p_text_new = p_text_new + p_text_1[1]

                                    slide_container.append(p_text_1[0])
                            
                                slide_array[slide_template_count-1] = p_text_new
                                
                            paragraph.text = p_text_new
                            del slides_cont[slide_edited_count]
                            slide_edited_count = slide_edited_count + 1
                            
            slide_template_count = slide_template_count+1




        presentation = SliderPresentation()
        presentation.put()
        presentation.template = template.key
        u = self.user
        presentation.owner = u.key

        kwargs = {}

        presentation.put()
        kwargs['presentation_id'] = presentation.key.id()
        kwargs['_full'] = True

        presentation_stream = BytesIO()
        prs.save(presentation_stream)

        presentation.pptx_file = presentation_stream.getvalue()
        presentation_url = self.uri_for('presentation_file', **kwargs)

        response_dict = presentation.__dict__()
        response_dict['url'] = presentation_url

        presentation.put()
        self.response_json(response_dict)
        

        
class TemplateAddDashboardHandler(BaseWebHandler):
    @user_required
    def get(self):
        self.render_template('presentation/content/dash_add_template.html')

    def post(self):
        slider_template = SliderBasicTemplate()
        slider_template.put()

        name = self.request.get('name')
        template_file = self.request.get('template_file')

        template_description = self.request.get('template_description')
        # fonts_str = self.request.get('fonts')
        # fonts = fonts_str.replace(' ', '').split(',')

        # for t in image_types:
        img = self.request.get('cover_img')
        if img:
            slider_template.set_image(img, 'cover_img')

        slider_template.template_file = template_file
        slider_template.name = name
        u = self.user
        slider_template.owner = u.key
        slider_template.template_description = template_description

        slider_template.put()

        self.redirect_to('dashboard')

class MyPresentationsWebHandler(BaseWebHandler):
    @user_required
    def get(self):
        u = self.user
        user_presentations = SliderPresentation.query(SliderPresentation.owner == u.key).fetch()
        presentations = []

        for presentation in user_presentations:
            presentation_dict = {'name': presentation.name, 'id': presentation.key.id()}
            presentations.append(presentation_dict)

        self.render_template('presentation/content/my_presentations.html', presentations = presentations)


class DashMyTemplatesHandler(BaseWebHandler):
    @user_required
    def get(self):
        u = self.user
        user_templates = SliderBasicTemplate.query(SliderBasicTemplate.owner == u.key).fetch()
        templates = []

        for template in user_templates:
            template_dict = {'name': template.name, 'id': template.key.id()}
            templates.append(template_dict)

       	self.render_template('presentation/content/my_templates.html', templates = templates)


class TemplatesListWindowHandler(BaseWebHandler):
    def post(self):
        templates = SliderTemplate.query().fetch()
        templates_descriptions = []

        i = 0
        for template in templates:
            
            template_dict = {'index':i,'name': template.name, 'fonts': template.fonts, 'id': template.key.id()}
            templates_descriptions.append(template_dict)
            i = i + 1

        self.render_template('presentation/content/templates.html', templates=templates_descriptions)


class GenerationWindowHandler(BaseWebHandler):
    def post(self):
        self.render_template('presentation/content/generation.html')


class SaveWindowHandler(BaseWebHandler):
    def post(self):
        pres_id = self.request.get('pres_id')
        self.render_template('presentation/content/save.html', pres_id=pres_id)


class GetPresentationHandler(BaseWebHandler):
    def post(self):
        text = self.request.get('text')
        template_id = self.request.get_range('template_id')
        user_agent = self.request.headers.get('User-Agent')
        images_count = self.request.get_range('images_count')

        template = SliderTemplate.get_by_id(template_id)

        u = self.user

        images = {} if images_count > 0 else None

        for i in xrange(images_count):
            img_name = 'image{0}'.format(i)
            images[img_name] = self.request.get(img_name)

        presentation_file, slide_array = make_presentation(text=text, template=template, images=images)
        presentation = SliderPresentation()
        presentation.put()
        presentation.template = template.key

        kwargs = {}

        if not u:
            presentation.owner = None
            token = SliderPresentationToken()
            token.presentation = presentation.key
            token.put()
            token.generate_token(user_agent)
            kwargs['token'] = token.token
            presentation.set_destruction_timer()
        else:
            presentation.owner = u.key

        for slide_dict in slide_array:
            slide_type = slide_dict['type']
            if slide_type == TITLE_SLIDE:
                presentation.add_slide(slide_type, slide_dict['title'], slide_dict['subtitle'])
            elif slide_type == TEXT_SLIDE:
                presentation.add_slide(slide_type, slide_dict['title'], slide_dict['text'])
            elif slide_type == HERO_SLIDE:
                presentation.add_slide(slide_type, None, slide_dict['text'])
            elif slide_type == TEXT_AND_PICTURE_SLIDE:
                image_name = slide_dict['image']
                presentation.add_slide(slide_type, slide_dict['title'], slide_dict['text'], image_name,
                                       images[image_name])
            elif slide_type == PICTURE_SLIDE:
                image_name = slide_dict['image']
                presentation.add_slide(slide_type, slide_dict['title'], None, image_name,
                                       images[image_name])
            elif slide_type == END_SLIDE:
                presentation.add_slide(slide_type, slide_dict['title'])

        presentation.put()

        kwargs['presentation_id'] = presentation.key.id()
        kwargs['_full'] = True

        presentation_stream = BytesIO()
        presentation_file.save(presentation_stream)
        presentation.pptx_file = presentation_stream.getvalue()

        self.response.write(presentation.key.id())

    def get(self):
        pres_id = self.request.get_range('pres_id')
        presentation = SliderPresentation.get_by_id(pres_id)

        pres_file = presentation.pptx_file

        self.response.headers['Content-Transfer-Encoding'] = 'binary'
        self.response.headers['Connection'] = 'Keep-Alive'
        self.response.headers['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0'
        self.response.headers['Content-Disposition'] = 'attachment; filename=' + str(presentation.name) + '.pptx'
        self.response.headers['File-Name'] = str(presentation.name)
        self.response.headers['presentation_id'] = str(presentation.key.id())
        self.response.headers[
            'ContentType'] = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        self.response.write(pres_file)
        return
