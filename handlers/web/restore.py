import logging
import os

import main
from handlers.web.base import BaseWebHandler
from methods.postmark import send_email
from models.user import User, ForgotPasswordToken
import webapp2
from google.appengine.api import taskqueue


class ForgotPasswordHandler(BaseWebHandler):
    def get(self):
        self.render_template('/auth/forgot.html')

    def post(self):
        email = self.request.get('email')

        u = User.get_by_auth_id(email)

        if not u:
            self.response.write('failure')
            return

        pass_token = ForgotPasswordToken()
        pass_token.put()
        pass_token.user = u.key
        pass_token.generate_token()
        pass_token.put()

        restoring_url = self.uri_for('restore_password', user_id=u.key.id(), token=pass_token.token, _full=True)

        restoring_html = os.path.join(os.path.dirname(main.__file__), 'templates', 'auth/restore_email.html')

        message = open(restoring_html).read().format(restoring_url)

        send_email('support@slider.pw', email, 'Restore password', message)

        taskqueue.add(url='/task/delete_password_token', method='POST', params={'pass_token': pass_token.token},
                      countdown=3600)

        self.response.write('success')


class RestorePasswordHandler(BaseWebHandler):
    def get(self):
        user_id = self.request.get_range('user_id')
        token = self.request.get('token')

        u = User.get_by_id(user_id)
        pass_token = ForgotPasswordToken.query(ForgotPasswordToken.token == token).get()

        if not u or not pass_token or pass_token.user != u.key:
            body = {
                'status': 'failure',
                'error': 'Invalid request.'
            }
            self.render_template('/auth/restore.html', **body)
            return

        body = {
            'status': 'success',
            'user_id': user_id,
            'token': token
        }

        self.render_template('/auth/restore.html', **body)

    def post(self):
        user_id = self.request.get_range('user_id')
        new_password = self.request.get('new_password')
        token = self.request.get('token')

        pass_token = ForgotPasswordToken.query(ForgotPasswordToken.token == token).get()

        user = User.get_by_id(user_id)

        if not user or not pass_token or pass_token.user != user.key:
            self.response.write('failure')
            return

        user.set_password(new_password)
        user.put()

        if pass_token:
            pass_token.key.delete()

        self.response.write('success')
