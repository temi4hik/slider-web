import logging
import os

from webapp2_extras import auth
from webapp2_extras import sessions
import main
from handlers.web.base import BaseWebHandler, user_required
from methods.postmark import send_email
from models.user import User
from models.presentation import SliderTemplate, SliderPresentation, SliderPresentationToken

class SignInHandler(BaseWebHandler):
    def get(self):
        self.render_template('/auth/sign_in.html')

    def post(self):
        email = self.request.get('email')
        password = self.request.get('password')

        try:
            u = self.auth.get_user_by_password(email, password, remember=True,
                                               save_session=True)
            self.redirect_to('presentation_main')
        except:
            logging.info('Login failed for user %s', email)

        self.__serve_page(True)

    def __serve_page(self, failed=False):
        email = self.request.get('email')
        params = {
            'email': email,
            'failed': failed
        }
        self.render_template('/auth/sign_in.html', **params)
        pass


class SignUpHandler(BaseWebHandler):
    def get(self):
        self.render_template('auth/sign_up.html')

    def post(self):
        email = self.request.get('email')
        name = self.request.get('name')
        last_name = self.request.get('last_name')
        password = self.request.get('password')

        unique_properties = ['email_address']

        user_data = self.user_model.create_user(email,
                                                unique_properties,
                                                email_address=email,
                                                name=name,
                                                password_raw=password,
                                                last_name=last_name,
                                                verified=False)

        logging.debug(user_data)

        if not user_data[0]:
            self.response.write('user_is_already_registered')
            return

        user = user_data[1]
        user_id = user.get_id()

        token = self.user_model.create_signup_token(user_id)

        verification_url = self.uri_for('verification', type='v', user_id=user_id,
                                        signup_token=token, _full=True)

        verification_html = os.path.join(os.path.dirname(main.__file__), 'templates', 'auth/verify_email.html')

        message = open(verification_html).read().format(verification_url)

        send_email('support@slider.pw', email, 'Confirmation', message)

        self.response.write('confirmation_sent')


class DashboardHandler(BaseWebHandler):
    @user_required
    def get(self):
        
        self.render_template('auth/Dore.Start.html')



class VerificationHandler(BaseWebHandler):
    def get(self):
        user = None
        user_id = self.request.get('user_id')
        signup_token = self.request.get('signup_token')
        verification_type = self.request.get('type')

        user, ts = self.user_model.get_by_auth_token(int(user_id), signup_token,
                                                     'signup')

        if not user:
            self.abort(404)

        self.auth.set_session(self.auth.store.user_to_dict(user), remember=True)

        if verification_type == 'v':
            # Disabling verification link
            self.user_model.delete_signup_token(user.get_id(), signup_token)

            if not user.verified:
                user.verified = True
                user.put()

class LogoutWebHandler(BaseWebHandler):
    def get(self):
        self.auth.unset_session()

        self.redirect(self.uri_for('main'))