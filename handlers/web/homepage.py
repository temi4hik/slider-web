from handlers.web.base import BaseWebHandler
from models.email import EmailInvite

import logging


class HomePageHandler(BaseWebHandler):
    def get(self):
        logging.debug(self.user)
        self.render_template('homepage/homepage.html')

    def post(self):
        email = self.request.get('email_slider_form')
        email_invite = EmailInvite()
        email_invite.email = email
        email_invite.put()
        self.response.write('success')
