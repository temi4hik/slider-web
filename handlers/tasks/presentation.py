import webapp2

from methods.presentation import delete_presentation
from models.presentation import SliderPresentation, SliderPresentationToken


class DeletePresentationTaskHandler(webapp2.RequestHandler):
    def post(self):
        user_agent = self.request.headers.get('User-Agent')

        if 'AppEngine-Google'.lower() not in user_agent.lower():
            return

        presentation_id = self.request.get_range('presentation_id')
        delete_presentation(presentation_id)
