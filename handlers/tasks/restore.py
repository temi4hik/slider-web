import logging

from models.user import ForgotPasswordToken
import webapp2


class DeleteForgotPasswordTokenHandler(webapp2.RequestHandler):
    def post(self):
        token = self.request.get('pass_token')

        user_agent = self.request.headers.get('User-Agent')

        if 'AppEngine-Google'.lower() not in user_agent.lower():
            return

        pass_token = ForgotPasswordToken.query(ForgotPasswordToken.token == token).get()

        if pass_token:
            pass_token.key.delete()
