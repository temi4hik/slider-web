import wikipedia
from base import BaseApiHandler, user_required, user_or_token_required
from models.presentation import SliderTemplate, SliderPresentation, SliderPresentationToken
from errors import WikiNoTitleError, WikiInvalidPageError
from requests_toolbelt.adapters import appengine
import logging
import libs.cloudstorage as gcs
from pptx import Presentation
from methods import wiki

appengine.monkeypatch()


class WikiSearchHandler(BaseApiHandler):
    def get(self):
        query = self.request.get('query', None)
        num_of_results = self.request.get('num', 20)
        language = self.request.get('language', 'en')

        wikipedia.set_lang(language)

        if query:
            results = wikipedia.search(query, results=num_of_results)
        else:
            results = wikipedia.random(pages=num_of_results)

        resp = {"results": results}

        self.response_json(resp)


class WikiSectionsHandler(BaseApiHandler):
    def get(self):
        page_title = self.request.get('title', None)

        if not page_title:
            self.response_error(WikiNoTitleError)

        language = self.request.get('language', 'en')
        wikipedia.set_lang(language)

        try:
            page = wikipedia.page(title=page_title, preload=True)
        except:
            self.response_error(WikiInvalidPageError)
            return

        response = {"sections": page.sections}

        self.response_json(response)

    def post(self):
        sections_str = self.request.get("sections")
        page = self.request.get("title")

        sections = sections_str.split('**')
        sections = map(unicode, sections)

        response = {"sections": sections}

        self.response_json(response)



class WikiMakePresentationHandler(BaseApiHandler):
    def post(self):
        page_title = self.request.get('title', None)

        if not page_title:
            self.response_error(WikiNoTitleError)
            return

        language = self.request.get('language', 'en')
        template_id = self.request.get_range('template_id')
        device_id = self.request.get('device_id')
        template = SliderTemplate.get_by_id(template_id)
        sections_str = self.request.get('sections')
        sections = sections_str.split('**')
        sections = map(unicode, sections)

        with gcs.open(template.template_file_path) as template_file:
            prs = Presentation(template_file)

        presentation = SliderPresentation()
        presentation.put()
        presentation.template = template.key

        u = self.user

        kwargs = {}

        if not u:
            presentation.owner = None
            token = SliderPresentationToken()
            token.presentation = presentation.key
            token.put()
            token.generate_token(device_id)
            kwargs['token'] = token.token
        else:
            presentation.owner = u.key

        presentation.set_destruction_timer()
        generator = wiki.WikiGenerator()
        presentation_stream = generator.MainPresentationGenerate(page_title, sections, language, prs)

        for slide in prs.slides:
            presentation.add_slide(1, "title", "text")

        presentation.put()
        kwargs['presentation_id'] = presentation.key.id()
        kwargs['_full'] = True

        presentation.pptx_file = presentation_stream.getvalue()
        presentation_url = self.uri_for('presentation_file', **kwargs)

        response_dict = presentation.__dict__()
        response_dict['url'] = presentation_url
        presentation.put()
        self.response_json(response_dict)
