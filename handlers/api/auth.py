from base import BaseApiHandler
from methods.postmark import send_email
import os
import main
import logging

from errors import AlreadyLoggedInAuthError, InvalidEmailOrPasswordAuthError, LogoutAuthError, \
    AlreadyRegisteredAuthError, AccessDeniedAuthError


class SignInApiHandler(BaseApiHandler):
    def post(self):
        if self.user:
            self.response_error(AlreadyLoggedInAuthError)
            return

        email = self.request.get('email')
        password = self.request.get('password')

        user = self.get_user(email, password)

        if user:
            self.response_json(user.__dict__())
        else:
            self.response_error(InvalidEmailOrPasswordAuthError)


class LogoutApiHandler(BaseApiHandler):
    def post(self):
        if not self.user:
            self.response_error(LogoutAuthError)
            return
        self.logout()

        self.response_json(None)


class SignUpApiHandler(BaseApiHandler):
    def post(self):
        email = self.request.get('email')
        name = self.request.get('name')
        last_name = self.request.get('last_name')
        password = self.request.get('password')

        unique_properties = ['email_address']

        user_data = self.user_model.create_user(email,
                                                unique_properties,
                                                email_address=email,
                                                name=name,
                                                password_raw=password,
                                                last_name=last_name,
                                                verified=False)

        if not user_data[0]:
            self.response_error(AlreadyRegisteredAuthError)
            return

        user = user_data[1]
        user_id = user.get_id()

        token = self.user_model.create_signup_token(user_id)

        verification_url = self.uri_for('verification', type='v', user_id=user_id,
                                        signup_token=token, _full=True)

        verification_html = os.path.join(os.path.dirname(main.__file__), 'templates', 'auth/verify_email.html')

        message = open(verification_html).read().format(verification_url)
        send_email('support@slider.pw', email, 'Confirmation', message)

        self.response_json(user.__dict__())


class UserInfoApiHandler(BaseApiHandler):
    def post(self):
        if not self.user:
            self.response_error(AccessDeniedAuthError)
        else:
            self.response_json(self.user.__dict__())


class UpdateUserInfoApiHandler(BaseApiHandler):
    def post(self):
        u = self.user

        if not u:
            self.response_error(AccessDeniedAuthError)
        else:
            name = self.request.get('name')
            last_name = self.request.get('last_name')

            if name:
                u.name = name
            if last_name:
                u.last_name = last_name

            u.put()

            self.response_json(u.__dict__())
