import logging
from . import *
from google.appengine.api import images
from webapp2 import RequestHandler

from handlers.api.base import BaseApiHandler
from models.presentation import SliderTemplate, image_types, SliderBasicTemplate


class TemplatesListHandler(BaseApiHandler):
    def get(self):
        templates = SliderTemplate.query().fetch()
        templates_descriptions = []
        for template in templates:

            template_imgs = {}

            for t in image_types:

                if t == 'cover_img':
                    t_uri_iphone = self.uri_for('template_image', template_id=template.key.id(),
                                                device_type='iPhone', image_type=t, _full=True)
                    t_uri_ipad = self.uri_for('template_image', template_id=template.key.id(),
                                              device_type='iPad', image_type=t, _full=True)

                    template_imgs[t] = {
                        'iPhone': t_uri_iphone,
                        'iPad': t_uri_ipad
                    }
                else:
                    image_uri = self.uri_for('template_image', template_id=template.key.id(),
                                             device_type='iPhone', image_type=t, _full=True)

                    template_imgs[t] = image_uri

            template_dict = template.__dict__()
            template_dict['images'] = template_imgs

            templates_descriptions.append(template_dict)

        self.response_json(templates_descriptions)


class TemplateImageHandler(RequestHandler):
    def get(self, template_id):
        template_id = int(template_id)
        template = SliderTemplate.get_by_id(template_id)

        device_type = self.request.get('device_type')
        image_type = self.request.get('image_type')
        image_type = image_type.lower()

        if not image_type:
            image_type = 'cover_img'

        crop = False

        if image_type == 'cover_img':
            ipad_size = (534, 374)
            iphone_size = (330, 190)
            crop = True
        else:
            ipad_size = (1080, 608)
            iphone_size = ipad_size

        img = template.get_image(image_type)
        if not img:
            return

        if device_type.lower() == 'ipad':
            img = images.resize(img, ipad_size[0], ipad_size[1], crop_to_fit=crop)
        else:
            img = images.resize(img, iphone_size[0], iphone_size[1])

        self.response.headers['Content-Type'] = content_types.PNG
        self.response.write(img)

class BasicTemplateImageHandler(RequestHandler):
    def get(self, template_id):
        template_id = int(template_id)
        template = SliderBasicTemplate.get_by_id(template_id)

        device_type = self.request.get('device_type')
        image_type = self.request.get('image_type')
        image_type = image_type.lower()

        if not image_type:
            image_type = 'cover_img'

        crop = False

        if image_type == 'cover_img':
            ipad_size = (534, 374)
            iphone_size = (330, 190)
            crop = True
        else:
            ipad_size = (1080, 608)
            iphone_size = ipad_size

        img = template.get_image(image_type)
        if not img:
            return

        if device_type.lower() == 'ipad':
            img = images.resize(img, ipad_size[0], ipad_size[1], crop_to_fit=crop)
        else:
            img = images.resize(img, iphone_size[0], iphone_size[1])

        self.response.headers['Content-Type'] = content_types.PNG
        self.response.write(img)

# class BasicTemplateFileHandler(BaseApiHandler):
#     @owner_required
#     def get(self, template_id):
#         token = self.request.get('token')
#         file_format = self.request.get('format')

#         file_format = 'pptx'

#         try:
#             template_id = int(template_id)
#         except ValueError:
#             self.response_error(InvalidRequestError)
#             return

#         u = self.user
#         presentation = SliderBasicTemplate.get_by_id(template_id)

#         if not presentation:
#             self.response_error(PresentationNotAvailableError)
#             return
#         elif presentation.owner != u.key:
#             self.response_error(PresentationAccessError)
#             return

#         pres_file = presentation.pptx_file if file_format == 'pptx' else presentation.pdf_file

#         if not pres_file and file_format == 'pdf':
#             self.response_error(PDFNotAvailableError)
#             return
#         elif not pres_file:
#             self.response_error(PresentationNotAvailableError)
#             return

#         file_name = '{0}.{1}'.format("Slider", file_format)
#         self.response.headers['Content-Disposition'] = 'attachment; filename="{0}"'.format(file_name)
#         self.response.headers['File-Name'] = file_name
#         self.response.headers['Content-Type'] = PPTX
#         self.response.headers['presentation_id'] = str(presentation.key.id())
#         self.response.write(pres_file)

#     @owner_required
#     def post(self, **kwargs):
#         file_format = self.request.get('format')

#         if not file_format or file_format.lower() not in ('pptx', 'pdf'):
#             self.response_error(InvalidRequestError)
#             return
#         file_format = file_format.lower()
#         presentation = kwargs['presentation']

#         new_file = self.request.get('file')

#         if file_format == 'pdf':
#             presentation.pdf_file = new_file
#         else:
#             presentation.pptx_file = new_file

#         self.response_json()
