from base import BaseApiHandler
from errors import UserDoesNotExistsError
from models.user import User, ForgotPasswordToken
import main
from google.appengine.api import taskqueue
import os
from methods.postmark import send_email


class ForgotPasswordApiHandler(BaseApiHandler):
    def post(self):
        email = self.request.get('email')

        u = User.get_by_auth_id(email)

        if not u:
            self.response_error(UserDoesNotExistsError)
            return

        pass_token = ForgotPasswordToken()
        pass_token.put()
        pass_token.user = u.key
        pass_token.generate_token()
        pass_token.put()

        restoring_url = self.uri_for('restore_password', user_id=u.key.id(), token=pass_token.token, _full=True)

        restoring_html = os.path.join(os.path.dirname(main.__file__), 'templates', 'auth/restore_email.html')

        message = open(restoring_html).read().format(restoring_url)

        send_email('support@slider.pw', email, 'Restore password', message)

        taskqueue.add(url='/task/delete_password_token', method='POST', params={'pass_token': pass_token.token},
                      countdown=3600)

        self.response_json(None)
