from io import BytesIO

from google.appengine.api.taskqueue import Queue

from base import BaseApiHandler, user_required, user_or_token_required
from errors import PresentationAccessError, PresentationNotAvailableError, AccessDeniedAuthError, \
    InvalidSlideImageError, \
    ImageNotAvailableError, InvalidRequestError, PDFNotAvailableError
from methods.presentation import delete_presentation
from methods.slide import TITLE_SLIDE, \
    TEXT_AND_PICTURE_SLIDE, TEXT_SLIDE, PICTURE_SLIDE, HERO_SLIDE, END_SLIDE
from methods.slide import make_presentation
from models.presentation import SliderTemplate, SliderPresentation, SliderPresentationToken
from content_types import PPTX

__author__ = 'Artem'


def owner_required(handler):
    def check_owner(self, **kwargs):
        if not self.user:
            self.response_error(InvalidRequestError)
        else:
            try:
                presentation_id = int(kwargs['presentation_id'])
            except ValueError:
                self.response_error(AccessDeniedAuthError)
            else:
                presentation = SliderPresentation.get_by_id(presentation_id)
                if self.user.key != presentation.owner:
                    self.response_error(PresentationAccessError)
                else:
                    return handler(self, presentation_id=presentation_id, presentation=presentation)

    return check_owner


class MakePresentationHandler(BaseApiHandler):
    def post(self):
        text = self.request.get('text')
        template_id = self.request.get_range('template_id')
        images_count = self.request.get_range('images_count')
        device_id = self.request.get('device_id')
        template = SliderTemplate.get_by_id(template_id)

        imgs = {} if images_count > 0 else None

        for i in xrange(images_count):
            img_name = 'image{0}'.format(i)
            imgs[img_name] = self.request.get(img_name)

        presentation_file, slide_array = make_presentation(text=text, template=template, images=imgs)
        presentation = SliderPresentation()
        presentation.put()
        presentation.template = template.key
        u = self.user

        kwargs = {}

        if not u:
            presentation.owner = None
            token = SliderPresentationToken()
            token.presentation = presentation.key
            token.put()
            token.generate_token(device_id)
            kwargs['token'] = token.token
        else:
            presentation.owner = u.key

        presentation.set_destruction_timer()

        for slide_dict in slide_array:
            slide_type = slide_dict['type']
            if slide_type == TITLE_SLIDE:
                presentation.add_slide(slide_type, slide_dict['title'], slide_dict['subtitle'])
            elif slide_type == TEXT_SLIDE:
                presentation.add_slide(slide_type, slide_dict['title'], slide_dict['text'])
            elif slide_type == HERO_SLIDE:
                presentation.add_slide(slide_type, None, slide_dict['text'])
            elif slide_type == TEXT_AND_PICTURE_SLIDE:
                image_name = slide_dict['image']
                presentation.add_slide(slide_type, slide_dict['title'], slide_dict['text'], image_name,
                                       imgs[image_name])
            elif slide_type == PICTURE_SLIDE:
                image_name = slide_dict['image']
                presentation.add_slide(slide_type, slide_dict['title'], None, image_name,
                                       imgs[image_name])
            elif slide_type == END_SLIDE:
                presentation.add_slide(slide_type, slide_dict['title'])

        presentation.put()
        kwargs['presentation_id'] = presentation.key.id()
        kwargs['_full'] = True

        presentation_stream = BytesIO()
        presentation_file.save(presentation_stream)

        presentation.pptx_file = presentation_stream.getvalue()
        presentation_url = self.uri_for('presentation_file', **kwargs)

        response_dict = presentation.__dict__()
        response_dict['url'] = presentation_url

        presentation.put()
        self.response_json(response_dict)


class PresentationFileHandler(BaseApiHandler):
    @user_or_token_required
    def get(self, presentation_id):
        token = self.request.get('token')
        file_format = self.request.get('format')

        if not file_format or file_format.lower() not in ('pptx', 'pdf'):
            file_format = 'pptx'
        else:
            file_format = file_format.lower()

        try:
            presentation_id = int(presentation_id)
        except ValueError:
            self.response_error(InvalidRequestError)
            return

        u = self.user
        presentation = SliderPresentation.get_by_id(presentation_id)

        if not presentation:
            self.response_error(PresentationNotAvailableError)
            return
        elif not presentation.owner:
            pr_token = SliderPresentationToken.query(SliderPresentationToken.token == token).get()

            if pr_token and pr_token.presentation == presentation.key:
                pass
            else:
                self.response_error(PresentationAccessError)
                return
        elif presentation.owner != u.key:
            self.response_error(PresentationAccessError)
            return

        pres_file = presentation.pptx_file if file_format == 'pptx' else presentation.pdf_file

        if not pres_file and file_format == 'pdf':
            self.response_error(PDFNotAvailableError)
            return
        elif not pres_file:
            self.response_error(PresentationNotAvailableError)
            return

        file_name = '{0}.{1}'.format("Slider", file_format)
        self.response.headers['Content-Disposition'] = 'attachment; filename="{0}"'.format(file_name)
        self.response.headers['File-Name'] = file_name
        self.response.headers['Content-Type'] = PPTX
        self.response.headers['presentation_id'] = str(presentation.key.id())
        self.response.write(pres_file)

    @owner_required
    def post(self, **kwargs):
        file_format = self.request.get('format')

        if not file_format or file_format.lower() not in ('pptx', 'pdf'):
            self.response_error(InvalidRequestError)
            return
        file_format = file_format.lower()
        presentation = kwargs['presentation']

        new_file = self.request.get('file')

        if file_format == 'pdf':
            presentation.pdf_file = new_file
        else:
            presentation.pptx_file = new_file

        self.response_json()


class DeletePresentationHandler(BaseApiHandler):
    @owner_required
    def post(self, **kwargs):
        presentation_id = kwargs['presentation_id']
        result = delete_presentation(presentation_id)

        if result:
            self.response_json()
        else:
            self.response_error(PresentationNotAvailableError)


class ConfirmPresentationHandler(BaseApiHandler):
    @owner_required
    def post(self, **kwargs):
        presentation_id = kwargs['presentation_id']
        queue = Queue()
        deleted_task = queue.delete_tasks_by_name(str(presentation_id))
        if deleted_task.was_deleted:
            self.response_json()
        else:
            self.response_json(PresentationNotAvailableError)


class PresentationImageHandler(BaseApiHandler):
    @owner_required
    def get(self, **kwargs):
        presentation = kwargs['presentation']
        slide = self.request.get_range('slide')

        if slide not in range(len(presentation.slides)):
            self.response_error(InvalidSlideImageError)
            return

        slide_preview_image = presentation.get_slide_preview_image(slide)

        if not slide_preview_image:
            self.response_error(ImageNotAvailableError)
            return

        self.response.headers['Content-Type'] = 'image/png'
        self.response.write(slide_preview_image)

    @owner_required
    def post(self, **kwargs):
        presentation = kwargs['presentation']

        slide = self.request.get('slide')

        if not slide:
            slides = range(len(presentation.slides))
        else:
            slides = [int(slide)]

        for slide in slides:
            image_data = self.request.get('slide{0}'.format(slide))
            presentation.set_slide_preview_image(slide, image_data)

        self.response_json()


class PresentationInfoHandler(BaseApiHandler):
    @owner_required
    def get(self, **kwargs):
        presentation = kwargs['presentation']
        self.response_json(presentation.__dict__())

    @owner_required
    def post(self, **kwargs):
        presentation = kwargs['presentation']
        new_name = self.request.get('name')
        presentation.name = new_name
        presentation.put()
        self.response_json(presentation.__dict__())


class UserPresentationsHandler(BaseApiHandler):
    @user_required
    def get(self):
        u = self.user
        user_presentations = SliderPresentation.query(SliderPresentation.owner == u.key).fetch()

        self.response_json([presentation.__dict__() for presentation in user_presentations])
