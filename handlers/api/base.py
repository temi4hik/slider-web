import webapp2
from webapp2 import RequestHandler, cached_property
from webapp2_extras import auth, sessions
import json

from errors import error_description, AccessDeniedAuthError


def user_required(handler):
    def check_login(self, *args, **kwargs):
        if not self.user:
            self.response_error(AccessDeniedAuthError)
        else:
            return handler(self, *args, **kwargs)

    return check_login


def user_or_token_required(handler):
    def check_login(self, *args, **kwargs):

        if not self.user and not self.request.get('token'):
            self.response_error(AccessDeniedAuthError)
        else:
            return handler(self, *args, **kwargs)

    return check_login


class FakeFloat(float):
    def __init__(self, value):
        self._value = value

    def __repr__(self):
        return self._value


class SJSONEncoder(json.JSONEncoder):
    def _replace(self, o):
        if isinstance(o, float):
            return FakeFloat("%.8f" % o)
        elif isinstance(o, dict):
            return {k: self._replace(v) for k, v in o.iteritems()}
        elif isinstance(o, (list, tuple)):
            return map(self._replace, o)
        else:
            return o

    def encode(self, o):
        return super(SJSONEncoder, self).encode(self._replace(o))


class BaseApiHandler(RequestHandler):
    @cached_property
    def auth(self):
        return auth.get_auth()

    @cached_property
    def user_info(self):
        return self.auth.get_user_by_session(True)

    @cached_property
    def user(self):
        u = self.user_info

        return self.user_model.get_by_id(u['user_id']) if u else None

    @cached_property
    def user_model(self):
        return self.auth.store.user_model

    @cached_property
    def session(self):
        return self.auth.session

    def logout(self):
        self.auth.unset_session()

    def get_user(self, email, password):
        try:
            u = self.auth.get_user_by_password(email, password, remember=True,
                                               save_session=True)
        except:
            u = None

        return self.user_model.get_by_id(u['user_id']) if u else None

    def response_json(self, obj=None):
        response_dict = {
            'meta': {
                'status': 'success'
            },
            'data': obj
        }

        self.response.headers["Content-Type"] = "application/json"
        self.response.write(json.dumps(response_dict, separators=(',', ':'), cls=SJSONEncoder))

    def response_error(self, error_code):

        error_str = error_description['en'][error_code]

        response_dict = {
            'meta': {
                'status': 'failure',
                'error_str': error_str,
                'error_code': error_code
            },
            'data': None
        }

        self.response.headers["Content-Type"] = "application/json"
        self.response.write(json.dumps(response_dict, separators=(',', ':'), cls=SJSONEncoder))

    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)
