from models.presentation import SliderPresentation, SliderPresentationToken


def delete_presentation(presentation_id):
    if not presentation_id:
        return False

    presentation = SliderPresentation.get_by_id(presentation_id)

    if not presentation:
        return False

    # check if there is token for this presentation
    if not presentation.owner:
        presentation_token = SliderPresentationToken \
            .query(SliderPresentationToken.presentation == presentation.key).get()
        if presentation_token:
            presentation_token.key.delete()

    del presentation.pptx_file
    del presentation.pdf_file

    i = 0

    for slide in presentation.slides:
        if slide.image_name:
            presentation.delete_image(slide.image_name)

        presentation.delete_slide_preview_image(i)
        i += 1

    presentation.key.delete()

    return True