import json
import logging
from google.appengine.api import urlfetch

URL = 'https://api.postmarkapp.com/email'


def send_email(from_email, to_email, subject, body, attachments=None, cc_email=None):
    if isinstance(to_email, list):
        to_email = ','.join(to_email)
    payloads = {
        'From': from_email,
        'To': to_email,
        'Subject': subject,
        'HtmlBody': body,
        'TrackOpens': False,
        'Attachments': attachments
    }

    if cc_email:
        if isinstance(cc_email, list):
            cc_email = ','.join(cc_email)
        payloads['Cc'] = cc_email
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Postmark-Server-Token': '6f93e6e0-eab8-4c87-9119-7d73c7548263'
    }
    response = urlfetch.fetch(URL, payload=json.dumps(payloads), headers=headers, method='POST')

    logging.debug('Sent presentation in send_email')

    return response.content
