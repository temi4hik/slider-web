# coding=utf-8
import logging

from pptx import Presentation
from pptx.enum.shapes import PP_PLACEHOLDER_TYPE
from models.slide import TextSlide, HeroSlide
from models.slide import TitleSlide, PictureSlide, TextAndPictureSlide, EndSlide
import re
import libs.cloudstorage as gcs
import StringIO

__author__ = 'Artem'

TITLE_SLIDE = 0
TEXT_SLIDE = 1
HERO_SLIDE = 2
TEXT_AND_PICTURE_SLIDE = 3
PICTURE_SLIDE = 4
END_SLIDE = 5

SLIDE_TYPES = (TITLE_SLIDE, TEXT_SLIDE, HERO_SLIDE, TEXT_AND_PICTURE_SLIDE, PICTURE_SLIDE, END_SLIDE)

slide_requirements = {
    TITLE_SLIDE: [PP_PLACEHOLDER_TYPE.TITLE, PP_PLACEHOLDER_TYPE.SUBTITLE],
    TEXT_SLIDE: [PP_PLACEHOLDER_TYPE.TITLE, PP_PLACEHOLDER_TYPE.BODY],
    HERO_SLIDE: [PP_PLACEHOLDER_TYPE.BODY],
    TEXT_AND_PICTURE_SLIDE: [PP_PLACEHOLDER_TYPE.TITLE, PP_PLACEHOLDER_TYPE.BODY, PP_PLACEHOLDER_TYPE.PICTURE],
    PICTURE_SLIDE: [PP_PLACEHOLDER_TYPE.TITLE, PP_PLACEHOLDER_TYPE.PICTURE],
    END_SLIDE: [PP_PLACEHOLDER_TYPE.TITLE]
}


def get_presentation_title_and_subtitle(first_slide, title_symbols, subtitle_symbols):
    sentences = []
    paragraphs = first_slide.split('\n')
    for paragraph in paragraphs:
        sentences += get_sentences(paragraph)

    title = ""
    subtitle = ""

    word_pattern = r"[\w'()]+|[.,!?; ]"

    words = []
    all_words = []
    first_sentence_words_num = len(re.findall(word_pattern, sentences[0], re.UNICODE))

    for sentence in sentences:
        words.extend(re.findall(word_pattern, sentence, re.UNICODE))

    # Getting title
    last_index = 0
    for i in range(0, first_sentence_words_num):
        if len(title) + len(words[i]) <= title_symbols:
            title += words[i]
        else:
            words = words[i:]
            break
    else:
        words = words[first_sentence_words_num:]

    # Getting subtitle
    for i in range(0, len(words)):
        if len(subtitle) + len(words[i]) <= subtitle_symbols:
            subtitle += words[i]
        else:
            words = words[i:]
            break
    else:
        words = []

    return re.sub(' +', ' ', title).strip(), re.sub(' +', ' ', subtitle).strip(), ''.join(words)


def get_slides(slide_text, template):
    slides = []

    # removing all extra newlines
    slide_text = re.sub(r'\n{3,}', '\n\n', slide_text.strip())

    slides_list = slide_text.split('\n\n')
    first_slide = slides_list.pop(0)

    title, subtitle, left_text = get_presentation_title_and_subtitle(first_slide, template.title_symbols,
                                                                     template.subtitle_symbols)

    slides.append({
        'title': title,
        'subtitle': subtitle,
        'type': TITLE_SLIDE
    })

    if len(left_text) > 0:
        slides_list += [left_text]

    for l in slides_list:
        sl_list = get_sl(l, template)
        slides += sl_list

    slides.append({
        'title': 'Thank you!',
        'type': END_SLIDE
    })

    return slides


def get_sl(slide_text, template):
    # first – look if there is image
    pattern = r'<picture>(.*?)</picture>'
    pictures = re.findall(pattern, slide_text, re.UNICODE)
    slides = []

    # If we found something – there are pictures and slide can be only picture-type
    if pictures:
        # Remove all occurrences of pictures
        slide_text = re.sub(pattern, '', slide_text).strip()
        slide_list = slide_text.split('\n')
        slide_title = get_title(slide_list[0], template.heading_symbols, template.heading_phrase_symbols)

        # check, if we put as title first paragraph
        if slide_title == slide_list[0]:
            # If yes – remove if from text.
            slide_list.pop(0)

        slide_text = '\n'.join(slide_list)

        for picture in pictures:
            if not slide_text:
                slides.append({
                    'title': slide_title,
                    'image': picture,
                    'type': PICTURE_SLIDE
                })
            else:
                picture_and_text_slide = divide_slide(slide_text, template.image_with_text_symbols).pop(0).strip()

                slides.append({
                    'title': slide_title,
                    'text': picture_and_text_slide,
                    'image': picture,
                    'type': TEXT_AND_PICTURE_SLIDE
                })

                slide_text = slide_text.strip().replace(picture_and_text_slide, '')

        # if there is some text left – divide it into text slides
        if len(slide_text):
            slide_arr = divide_slide(slide_text, template.slide_symbols)

            for sl in slide_arr:
                slides.append({
                    'title': slide_title,
                    'type': TEXT_SLIDE,
                    'text': sl
                })
    else:
        slide_list = slide_text.split('\n')

        if len(slide_list) == 1 and len(slide_list[0]) <= template.hero_symbols:
            # We have hero slide. It does not need title, just returning it
            slides.append({
                'text': slide_text,
                'type': HERO_SLIDE
            })
        else:
            # Otherwise we have text slide

            # Getting title from first paragraph
            slide_title = get_title(slide_list[0], template.heading_symbols, template.heading_phrase_symbols)

            # If first paragraph was small to become title – removing it.
            if slide_title == slide_list[0]:
                slide_list.pop(0)

            slide_text = '\n'.join(slide_list)

            if len(slide_text) <= template.slide_symbols:
                slides.append({
                    'text': slide_text,
                    'title': slide_title,
                    'type': TEXT_SLIDE
                })
            else:
                slide_arr = divide_slide(slide_text, template.slide_symbols)

                for sl in slide_arr:
                    slides.append({
                        'text': sl,
                        'title': slide_title,
                        'type': TEXT_SLIDE
                    })

    return slides


def get_title(text, heading_symbols, heading_phrase_symbols):
    if len(text) <= heading_symbols:
        return text
    else:
        sentences = get_sentences(text)

        # if length of sentence is less than 70 symbols then it becomes title
        if len(sentences[0]) <= heading_symbols:
            return sentences[0]
        # else we choose first words less than 20 symbols
        else:
            words = sentences[0].split(" ")
            title_list = []
            i = 0
            total_len = 0
            while total_len + len(words[i]) <= heading_phrase_symbols:
                title_list.append(words[i])
                total_len += len(words[i])
                i += 1

            return ' '.join(title_list).rstrip(',')


def divide_slide(slide_text, slide_symbols):
    """
    Function divides one big text into several smaller
    :param slide_symbols:
    :param slide_text: text to divide
    :return array of divided texts
    """
    text_length = len(slide_text)
    slices_num = [text_length / slide_symbols + 1,
                  text_length / slide_symbols][text_length % slide_symbols == 0]

    sentences = get_sentences(slide_text)

    slide_arr = []

    sentences_per_slide = len(sentences) / slices_num

    for i in range(0, slices_num):
        k = i * sentences_per_slide

        slide_str = ""
        for a in range(0, sentences_per_slide):
            slide_str += sentences[k + a] + " "

        slide_arr.append(slide_str)

    return slide_arr


def get_sentences(text):
    """
    Divides text into list of sentences
    :param text: text to divide
    :return: list of sentences
    """
    s = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', text, re.UNICODE)
    return [sentence + ' ' for sentence in s]


def _get_layout(prs, sld_type):
    if sld_type not in SLIDE_TYPES:
        return None

    layouts = prs.slide_layouts

    for layout in layouts:
        # getting all placeholder types in layout
        layout_placeholders = [placeholder.placeholder_format.type for placeholder in layout.placeholders]

        # if current layout satisfy requirements – return it
        if set(layout_placeholders) == set(slide_requirements[sld_type]):
            return layout

    return None


def get_slide(prs, sld_type):
    """Function to get slide of some type
    :returns slide:
    :param sld_type: string parameter which defines slide type. Allowable values locate in slide_types
    :raises TypeError if sld_type not in slide_type
    """
    if sld_type not in SLIDE_TYPES:
        raise TypeError('Invalid type of slide!')

    slide_layout = _get_layout(prs, sld_type)
    slide = prs.slides.add_slide(slide_layout)

    if sld_type == TITLE_SLIDE:
        return TitleSlide(slide)
    elif sld_type == TEXT_SLIDE:
        return TextSlide(slide)
    elif sld_type == PICTURE_SLIDE:
        return PictureSlide(slide)
    elif sld_type == HERO_SLIDE:
        return HeroSlide(slide)
    elif sld_type == TEXT_AND_PICTURE_SLIDE:
        return TextAndPictureSlide(slide)
    elif sld_type == END_SLIDE:
        return EndSlide(slide)


def make_presentation(text, template, images=None):
    with gcs.open(template.template_file_path) as template_file:
        prs = Presentation(template_file)

    slides = get_slides(text, template)

    for slide_dict in slides:

        slide_type = slide_dict['type']

        slide = get_slide(prs, slide_type)

        if slide_type == TITLE_SLIDE:
            slide.title = slide_dict['title']
            slide.subtitle = slide_dict['subtitle']
            continue

        if slide_type == END_SLIDE:
            slide.title = slide_dict['title']
            continue

        if slide_type in (TEXT_SLIDE, PICTURE_SLIDE, TEXT_AND_PICTURE_SLIDE):
            slide.title = slide_dict['title']

        if slide_type in (TEXT_SLIDE, TEXT_AND_PICTURE_SLIDE, HERO_SLIDE):
            slide.text = slide_dict['text']

        if slide_type in (TEXT_AND_PICTURE_SLIDE, PICTURE_SLIDE):
            image_io = StringIO.StringIO(images[slide_dict['image']])
            slide._picture.insert_picture(image_io)
            image_io.close()

    return prs, slides
