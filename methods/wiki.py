# -*- coding: utf-8 -*-

from pptx import Presentation
from pptx.enum.text import MSO_ANCHOR
from pptx.util import Pt
from bs4 import BeautifulSoup
from io import BytesIO
import ssl
import StringIO
import urllib2
import requests
import re
import math
import logging


class WikiGenerator(object):
    section_current_name = ''
    imagelink_check = '0'
    summary = []
    maintitle = ''
    language_t = ''
    Mainimage_array = []
    image_get_width = '650px'
    unexcepted_images = 'https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Question_book-new.svg/' + image_get_width + '-Question_book-new.svg.png'
    page_data = {
        "title": maintitle,
        "summary_paragraphs": summary,
        "summary_images": Mainimage_array,
        "sections": []
    }
    paragraphs_text = ''


    def download_web_image(self, url):
        request = urllib2.Request(url)
        context = ssl._create_unverified_context()
        img_data = urllib2.urlopen(request, context=context).read()
        img_io = StringIO.StringIO(img_data)
        return img_io


    def replace_with_image(self, img, shape, slide):
        pic = slide.shapes.add_picture(img, shape.left, shape.top)

        # calculate max width/height for target size
        ratio = min(shape.width / float(pic.width), shape.height / float(pic.height))

        pic.height = int(pic.height * ratio)
        pic.width = int(pic.width * ratio)

        pic.left = shape.left + ((shape.width - pic.width) / 2)
        pic.top = shape.top + ((shape.height - pic.height) / 2)

        placeholder = shape.element
        placeholder.getparent().remove(placeholder)
        return


    def get_bestFont(self, text_input, placeholder_width, placeholder_height, placeholder_type, font_size=18, iteration=0):
        p_type = placeholder_type.split(' ')
        text_input_splitted = text_input.split(' ')

        k_temp = 0.55
        len_text_input = len(text_input)
        
        if len_text_input > 100 and len_text_input <= 150 :
            if self.language_t == 'ru':
                k_temp = 0.50
            else:
                k_temp = 0.48

        elif len_text_input > 150 and len_text_input <= 500:
            if self.language_t == 'ru':
                k_temp = 0.49
            else:
                k_temp = 0.47

        elif len_text_input > 500 and len_text_input <= 700: 
            if self.language_t == 'ru':
                k_temp = 0.48
            else:
                k_temp = 0.46

        elif len_text_input > 600 and len_text_input <= 900: 
            if self.language_t == 'ru':
                k_temp = 0.47
            else:
                k_temp = 0.45

        elif len_text_input > 900 and len_text_input <= 1200: 
            if self.language_t == 'ru':
                k_temp = 0.46
            else:
                k_temp = 0.44

        elif len_text_input > 10 and len_text_input <= 30: 
            k_temp = 0.60
        else:
            k_temp = 0.60

        mcl = placeholder_width // ((font_size / 72.0) * 914400.0 * k_temp)  # counting maximum character per length
        mpl = placeholder_height // ((font_size / 72.0) * 914400.0)  # counting maximum placeholder lines
        mcl_temp = mcl
        mpl_temp = mpl

        best_font = font_size
        # Render Loop
        for word in text_input_splitted:
            if (mcl_temp - len(word) - 1) > 0:
                mcl_temp = mcl_temp - len(word) - 1
            elif mpl_temp > 1:
                mpl_temp = mpl_temp - 1
                mcl_temp = mcl - len(word) - 1
            else:
                if iteration:
                    return best_font - 1

                best_font = self.get_bestFont(text_input, placeholder_width, placeholder_height, placeholder_type,
                                         (font_size - 1))
                return best_font

        if mpl_temp >= 1:
            best_font = self.get_bestFont(text_input, placeholder_width, placeholder_height, placeholder_type, (font_size + 1),
                                     'increase')

        return best_font


    # ----- function for dividing paragraphs into two ------#
    def chunkIt(self, seq):
        seq = seq.split('. ')
        count = len(seq)
        logging.debug(count)

        count_half1 = ''
        count_half2 = ''

        if count % 2 == 0:
            count_half1 = count_half2 = count / 2
        else:
            count_half1 = int(math.floor(count / 2))
            count_half2 = int(round(count / 2) + 1)

        array_text_half1 = []
        array_text_half2 = []

        for x in range(0, count_half1):
            array_text_half1.append(seq[x])
            logging.debug(array_text_half1)

        for y in range(count_half1, count_half1 + count_half2):
            array_text_half2.append(seq[y])
            logging.debug(array_text_half2)

        seq = []
        seq.append('. '.join([x for x in array_text_half1]) + '. ')
        seq.append('. '.join([x for x in array_text_half2]))
        logging.debug(seq)

        return seq


    def get_title_images_by_link(self, link):
        html = requests.get(link)
        maindata = html.text
        c = BeautifulSoup(html.text, 'html.parser')
        images = []
        if 'class="image"' in maindata:

            try:
                mainimage1 = c.find("table", {"class": "infobox"})
                mainimage1 = mainimage1.find_all("a", {"class": "image"})

                for x in range(0, len(mainimage1)):
                    try:
                        Mainimage = str(mainimage1[x].img).split('src="', 1)[1]
                        Mainimage = Mainimage.split('"', 1)[0]
                        imagelink = ''

                        imagelink_temp = Mainimage.split('/')
                        test = imagelink_temp[-1].split('px-')
                        if len(test) > 1:
                            imagelink_temp_size = imagelink_temp[-1].split('-')
                            del imagelink_temp[-1]
                            imagelink_temp_size[0] = self.image_get_width
                            imagelink_temp_size = '-'.join([str(x) for x in imagelink_temp_size])
                            imagelink_temp.append(imagelink_temp_size)
                            imagelink_temp = '/'.join([str(x) for x in imagelink_temp])
                            imagelink = 'https:' + str(imagelink_temp)
                        else:
                            imagelink = 'https:' + Mainimage

                        if imagelink != self.unexcepted_images and '.svg' not in imagelink:
                            images.append(imagelink)

                    except Exception as e:
                        pass

            except Exception as e:
                pass

            try:
                abc = maindata.find_all("a", {"class": "image"})
                for k in range(0, len(abc)):
                    imagelink = str(abc[k].img).split('src="', 1)[1]
                    imagelink = imagelink.split('"', 1)[0]
                    imagelink_temp = imagelink.split('/')

                    imagelink_temp_size = imagelink_temp[-1].split('-')
                    del imagelink_temp[-1]
                    imagelink_temp_size[0] = self.image_get_width
                    imagelink_temp_size = '-'.join([x for x in imagelink_temp_size])
                    imagelink_temp.append(imagelink_temp_size)
                    imagelink_temp = '/'.join([x for x in imagelink_temp])

                    imagelink = 'https:' + str(imagelink_temp)
                    if imagelink != self.unexcepted_images and '.svg' not in imagelink:
                        images.append(imagelink)

            except Exception as e:
                pass

        return images


    def find_new_images(self, paragraph0):
        image_links = []
        link_texts = []
        # logging.debug(paragraph0)
        # logging.debug(type(paragraph0))
        # try:
        #     paragraph = BeautifulSoup(paragraph0.text,'html.parser')
        #     paragraph = paragraph.find_all('a')
        #     for link in paragraph:
        #         temp_title = link.text
        #         logging.debug(temp_title)
        #         try:
        #             if temp_title[0].isupper():
        #                 link_temp = 'https://en.wikipedia.org' + link.get('href')
        #                 temp_temp_img_link = self.get_title_images_by_link(link_temp)
        #                 try:
        #                     if temp_temp_img_link:
        #                         logging.debug(temp_temp_img_link)
        #                         image_links.append(temp_temp_img_link[0])
        #                         link_texts.append(temp_title)
        #                 except Exception as e:
        #                     logging.debu(e)
        #                     pass
        #                     # print(e)
        #         except Exception as e:
        #             logging.debug(e)
        #             pass
        # except Exception as e:
        #     logging.debug(e)
        #     pass

        try:
            temp = str(paragraph0).split('<a')
            for x in temp:
                if '</a>' in x:
                    x = x.split('>')
                    x_title = x[1].split('</a')
                    if x_title[0][0].isupper():
                        x_link = 'https://en.wikipedia.org' + x[0].split('href="')[1].split('"')[0]
                        try:
                            temp_temp_img_link = self.get_title_images_by_link(x_link)
                            if temp_temp_img_link:
                                logging.debug(temp_temp_img_link)
                                image_links.append(temp_temp_img_link[0])
                                link_texts.append(x_title[0])
                        except Exception as e:
                            logging.debu(e)
                            pass
        except Exception as e:
            logging.debug(e)
            pass

        images = [link_texts, image_links]
        return images


    def split_paragraph_html_with_images(self, paragraph_html, paragraph_pure):

        new_paragraphs_pure = self.chunkIt(paragraph_pure)
        logging.debug(new_paragraphs_pure)
        new_paragraphs_html = self.chunkIt(unicode(paragraph_html))
        new_paragraphs_images = []

        if len(new_paragraphs_pure[1]) > 400:
            new_paragraphs_pure2 = self.chunkIt(new_paragraphs_pure[1])
            new_paragraphs_html2 = self.chunkIt(unicode(new_paragraphs_html[1]))
            logging.debug(new_paragraphs_pure2)

            new_paragraphs_pure[1] = new_paragraphs_pure2[0]
            new_paragraphs_pure.append(new_paragraphs_pure2[1])

            new_paragraphs_html[1] = new_paragraphs_html2[0]
            new_paragraphs_html.append(new_paragraphs_html2[1])

        for k in range(0, len(new_paragraphs_html)):
            image_slide = self.find_new_images(new_paragraphs_html[k])
            logging.debug(image_slide)
            try:
                formaing_title_link = [image_slide[0][0], image_slide[1][0]]
                new_paragraphs_images.append(formaing_title_link)
            except Exception as e:
                formaing_title_link = ['', '']
                new_paragraphs_images.append(formaing_title_link)
                

        array_slides = [new_paragraphs_pure, new_paragraphs_images]
        logging.debug(array_slides)
        return array_slides


    def regular_slide_text_title_image(self, title, text_paragraph, imagelink_s, prs):
        if len(text_paragraph) > 400:

            if len(text_paragraph) > 600:
                # text_paragraph = text_paragraph.replace(u'\xa0', u' ')
                divided_paragraph = self.chunkIt(text_paragraph)
                self.regular_slide_text_title_image(title, divided_paragraph[0], imagelink_s, prs)
                self.regular_slide_text_title_image(title, divided_paragraph[1], imagelink_s, prs)

            else:
                slide = prs.slides.add_slide(prs.slide_layouts[6])
                # ------ Inserting Title
                text_frame = slide.placeholders[0].text_frame
                text_frame.clear()
                text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
                run = text_frame.paragraphs[0].add_run()
                run.text = test_text = title
                autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                        slide.placeholders[0].name)
                font = run.font
                font.size = Pt(autofont)

                # -------- Inserting paragraph
                text_frame2 = slide.placeholders[14].text_frame
                text_frame2.clear()
                text_frame2.vertical_anchor = MSO_ANCHOR.MIDDLE
                run = text_frame2.paragraphs[0].add_run()
                run.text = test_text = text_paragraph
                autofont = self.get_bestFont(test_text, slide.placeholders[14].width, slide.placeholders[14].height,
                                        slide.placeholders[14].name)
                font = run.font
                font.size = Pt(autofont)

                if imagelink_s:
                    link = imagelink_s
                    placeholder = slide.placeholders[13]  # idx key, not position
                    self.replace_with_image(self.download_web_image(link), placeholder, slide)
                    self.imagelink_check = imagelink_s

        else:
            slide = prs.slides.add_slide(prs.slide_layouts[0])
            # ------ Inserting Title
            text_frame = slide.placeholders[0].text_frame
            text_frame.clear()
            text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
            run = text_frame.paragraphs[0].add_run()
            run.text = test_text = title
            autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                    slide.placeholders[0].name)
            font = run.font
            font.size = Pt(autofont)

            # -------- Inserting paragraph
            text_frame2 = slide.placeholders[14].text_frame
            text_frame2.clear()
            text_frame2.vertical_anchor = MSO_ANCHOR.MIDDLE
            run = text_frame2.paragraphs[0].add_run()
            run.text = test_text = text_paragraph
            autofont = self.get_bestFont(test_text, slide.placeholders[14].width, slide.placeholders[14].height,
                                    slide.placeholders[14].name)
            font = run.font
            font.size = Pt(autofont)

            if imagelink_s:
                link = imagelink_s
                placeholder = slide.placeholders[13]  # idx key, not position
                self.replace_with_image(self.download_web_image(link), placeholder, slide)
                self.imagelink_check = imagelink_s


    def regular_slide_text_title_image_caption(self, title, text_paragraph, imagelink_s, caption, prs):

        slide = prs.slides.add_slide(prs.slide_layouts[7])
        # ------ Inserting Title
        text_frame = slide.placeholders[0].text_frame
        text_frame.clear()
        text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
        run = text_frame.paragraphs[0].add_run()
        run.text = test_text = title
        autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                slide.placeholders[0].name)
        font = run.font
        font.size = Pt(autofont)

        # -------- Inserting paragraph
        text_frame2 = slide.placeholders[14].text_frame
        text_frame2.clear()
        text_frame2.vertical_anchor = MSO_ANCHOR.MIDDLE
        run = text_frame2.paragraphs[0].add_run()
        run.text = test_text = text_paragraph
        autofont = self.get_bestFont(test_text, slide.placeholders[14].width, slide.placeholders[14].height,
                                slide.placeholders[14].name)
        font = run.font
        font.size = Pt(autofont)

        if imagelink_s != '':
            link = imagelink_s
            placeholder = slide.placeholders[13]  
            self.replace_with_image(self.download_web_image(link), placeholder, slide)
            self.imagelink_check = imagelink_s

        # -------- Inserting caption
        text_frame2 = slide.placeholders[15].text_frame
        text_frame2.clear()
        text_frame2.vertical_anchor = MSO_ANCHOR.TOP
        run = text_frame2.paragraphs[0].add_run()
        run.text = test_text = caption
        autofont = self.get_bestFont(test_text, slide.placeholders[15].width, slide.placeholders[15].height,
                                slide.placeholders[15].name)
        font = run.font
        font.size = Pt(autofont)


    def image_slide(self, text_paragraph, imagelink_s, prs):
        slide = prs.slides.add_slide(prs.slide_layouts[2])
        space = slide.placeholders[0].width * slide.placeholders[0].height
        text_frame = slide.placeholders[0].text_frame

        text_frame.clear()
        text_frame.vertical_anchor = MSO_ANCHOR.TOP
        run = text_frame.paragraphs[0].add_run()
        run.text = test_text = text_paragraph
        autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                slide.placeholders[0].name)

        font = run.font
        font.size = Pt(autofont)

        if imagelink_s:
            link = imagelink_s
            placeholder = slide.placeholders[13]  # idx key, not position
            self.replace_with_image(self.download_web_image(link), placeholder, slide)
            self.imagelink_check = imagelink_s


    def text_slide(self, title, paragraphs_text_pure, prs):
        slide = prs.slides.add_slide(prs.slide_layouts[4])

        # ------ Inserting Title
        text_frame = slide.placeholders[0].text_frame
        text_frame.clear()
        text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
        run = text_frame.paragraphs[0].add_run()
        run.text = test_text = title
        autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                slide.placeholders[0].name)
        font = run.font
        font.size = Pt(autofont)

        # -------- Inserting paragraph
        text_frame2 = slide.placeholders[14].text_frame
        text_frame2.clear()
        text_frame2.vertical_anchor = MSO_ANCHOR.MIDDLE
        run = text_frame2.paragraphs[0].add_run()
        run.text = test_text = paragraphs_text_pure
        autofont = self.get_bestFont(test_text, slide.placeholders[14].width, slide.placeholders[14].height,
                                slide.placeholders[14].name)
        font = run.font
        font.size = Pt(autofont)


    def hero_slide(self, title, imagelink_s, prs):

        slide = prs.slides.add_slide(prs.slide_layouts[3])
        # ------ Inserting image hero if exists
        if imagelink_s:
            link = imagelink_s
            placeholder = slide.placeholders[13]  # idx key, not position
            placeholder.insert_picture(self.download_web_image(link))

        # ------ Inserting Title
        text_frame = slide.placeholders[0].text_frame
        text_frame.clear()
        text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
        run = text_frame.paragraphs[0].add_run()
        run.text = test_text = title
        autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                slide.placeholders[0].name)
        font = run.font
        font.size = Pt(autofont)


    def title_slide(self, title, imagelink_s, prs):
        if len(self.Mainimage_array) == 1:
            # ------ Title slide
            slide = prs.slides.add_slide(prs.slide_layouts[5])
            
            link = self.Mainimage_array[0]
            placeholder = slide.placeholders[10]  
            try:
                self.replace_with_image(self.download_web_image(link), placeholder, slide)
            except:
                pass


            # Getting data about the placeholders
            space = slide.placeholders[0].width * slide.placeholders[0].height
            # Inserting text
            text_frame = slide.placeholders[0].text_frame

            text_frame.clear()
            text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
            run = text_frame.paragraphs[0].add_run()
            run.text = test_text = self.maintitle  # here get the text
            autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                    slide.placeholders[0].name)
            if autofont>36:
                autofont = 36
            font = run.font
            font.size = Pt(autofont)
            

        elif not self.Mainimage_array:
            slide = prs.slides.add_slide(prs.slide_layouts[3])

            # ------ Inserting Title
            text_frame = slide.placeholders[0].text_frame
            text_frame.clear()
            text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
            run = text_frame.paragraphs[0].add_run()
            run.text = test_text = self.maintitle 
            autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                    slide.placeholders[0].name)
            font = run.font
            font.size = Pt(autofont)

        else:
            # ------ Title slide
            slide = prs.slides.add_slide(prs.slide_layouts[1])
            # Getting data about the placeholders
            space = slide.placeholders[0].width * slide.placeholders[0].height
            # Inserting text
            text_frame = slide.placeholders[0].text_frame

            text_frame.clear()
            text_frame.vertical_anchor = MSO_ANCHOR.MIDDLE
            run = text_frame.paragraphs[0].add_run()
            run.text = test_text = self.maintitle  # here get the text
            autofont = self.get_bestFont(test_text, slide.placeholders[0].width, slide.placeholders[0].height,
                                    slide.placeholders[0].name)
            font = run.font
            font.size = Pt(autofont)

            for x in range(0, len(self.Mainimage_array)):
                link = self.Mainimage_array[x]
                placeholder = slide.placeholders[10 + x]  # idx key, not position
                try:
                    self.replace_with_image(self.download_web_image(link), placeholder, slide)
                except:
                    pass

                if x == 2:
                    break


    def GetMoreImages(self, title2, paragraphs_text_pure, paragraphs_text_html, prs):
        # print('FUNC GetMoreImages')
        logging.debug(paragraphs_text_html)
        if len(paragraphs_text_pure) > 400:
            array_slides = self.split_paragraph_html_with_images(paragraphs_text_html, paragraphs_text_pure)
            logging.debug(array_slides)
            for x in range(0, len(array_slides[0])):
                if array_slides[1][x][0] != '':  # array_slides[x][1][0] - это caption надпись картинки
                    self.regular_slide_text_title_image_caption(title2, array_slides[0][x], array_slides[1][x][1],
                                                           array_slides[1][x][0], prs)
                else:
                    self.text_slide(title2, array_slides[0][x], prs)
        else:
            try:
                imagelinks = self.find_new_images(paragraphs_text_html)
                logging.debug(imagelinks)
                self.regular_slide_text_title_image_caption(title2, paragraphs_text_pure, imagelinks[1][0], imagelinks[0][0],
                                                            prs)
            except Exception as e:
                self.text_slide(title2, paragraphs_text_pure, prs)
                logging.debug(e)
                # print(e)


    def MainPresentationGenerate(self, title_0, sections_0, language_0, prs):
        # title = "Friedrich Nietzsche"
        # sections_array_list_t = ['Critique of mass culture', 'Übermensch']

        logging.debug('Presentation Started')

        title = title_0

        self.Mainimage_array = []
        self.language_t = language_0
        sections_array_list = []

        for x in range(0, len(sections_0)):
            sections_array_list.append(sections_0[x])
            # sections_array_list.append(sections_0[x].decode('utf-8'))

        page_url = u"https://{0}.wikipedia.org/wiki/{1}".format(language_0, title)

        html = requests.get(page_url)

        maindata = html.text
        # maindata = unicode(unicodedata.normalize('NFKD',maindata).encode('ascii', 'ignore'))
        b = BeautifulSoup(html.text, 'html.parser')

        # ------ wiki summary web-scraping code starts here -----------#
        self.maintitle = b.find("h1", {"id": "firstHeading"})
        self.maintitle = self.maintitle.text.strip()
        summary_full = maindata.split('<h1', 1)[1]
        summary_full = summary_full.split('<h2>', 1)[0]
        cd = BeautifulSoup(summary_full, 'html.parser')
        abcd = cd.find_all('p')

        # ------ reformatting image links into full width source links of the summary block
        if 'class="image"' in maindata:

            try:
                mainimage1 = b.find("table", {"class": "infobox"})
                mainimage1 = mainimage1.find_all("a", {"class": "image"})

                for x in range(0, len(mainimage1)):
                    try:
                        Mainimage = str(mainimage1[x].img).split('src="', 1)[1]
                        Mainimage = Mainimage.split('"', 1)[0]
                        imagelink = ''

                        imagelink_temp = Mainimage.split('/')
                        test = imagelink_temp[-1].split('px-')
                        if len(test) > 1:
                            imagelink_temp_size = imagelink_temp[-1].split('-')
                            del imagelink_temp[-1]
                            imagelink_temp_size[0] = self.image_get_width
                            imagelink_temp_size = '-'.join([str(x) for x in imagelink_temp_size])
                            imagelink_temp.append(imagelink_temp_size)
                            imagelink_temp = '/'.join([str(x) for x in imagelink_temp])
                            imagelink = 'https:' + str(imagelink_temp)
                        else:
                            imagelink = 'https:' + Mainimage

                        if imagelink != self.unexcepted_images and '.svg' not in imagelink:
                            # print('check svg - ' + imagelink)
                            self.Mainimage_array.append(imagelink)



                    except Exception as e:
                        pass
                        
            except Exception as e:
                
                pass
                

            try:
                abc = maindata.find_all("a", {"class": "image"})
                # print('abc')
                for k in range(0, len(abc)):
                    imagelink = str(abc[k].img).split('src="', 1)[1]
                    imagelink = imagelink.split('"', 1)[0]


                    imagelink_temp = imagelink.split('/')
                    test = imagelink_temp[-1].split('px-')
                    if len(test) > 1:
                        imagelink_temp_size = imagelink_temp[-1].split('-')
                        del imagelink_temp[-1]
                        imagelink_temp_size[0] = self.image_get_width
                        imagelink_temp_size = '-'.join([str(x) for x in imagelink_temp_size])
                        imagelink_temp.append(imagelink_temp_size)
                        imagelink_temp = '/'.join([str(x) for x in imagelink_temp])
                        imagelink = 'https:' + str(imagelink_temp)
                    else:
                        imagelink = 'https:' + imagelink


                    if imagelink != self.unexcepted_images and '.svg' not in imagelink:
                        self.Mainimage_array.append(imagelink)

            except Exception as e:
                print(e)
                pass

        # ------ code for inserting summary slides starts here -----------#
        # ----- title slide ------#

        self.title_slide(self.maintitle, self.Mainimage_array, prs)

        for g in range(0, len(abcd)):

            paragraphs_text = abcd[g].text.strip()
            paragraphs_text = paragraphs_text.split('[')
            paragraphs_text_pure = ''
            if len(paragraphs_text) > 1:
                for v in range(0, len(paragraphs_text)):
                    paragraphs_text[v] = paragraphs_text[v].split(']')
                    if len(paragraphs_text[v]) > 1:
                        paragraphs_text_pure = paragraphs_text_pure + paragraphs_text[v][1]

                    else:
                        paragraphs_text_pure = paragraphs_text_pure + paragraphs_text[v][0]
            else:
                paragraphs_text_pure = paragraphs_text[0]
            self.summary.append(paragraphs_text_pure)


            # ------ inserting summary paragraphs ----------- #
            if len(paragraphs_text_pure) > 0:
                # ------ IF needs to be used regular layout text title slide -----------#
                if len(paragraphs_text_pure) > 200 and self.Mainimage_array:
                    if self.imagelink_check == imagelink:
                        logging.debug(abcd[g])
                        self.GetMoreImages(self.maintitle, paragraphs_text_pure, abcd[g], prs)

                    else:
                        logging.debug(abcd[g])
                        imagelink_check = imagelink
                        self.regular_slide_text_title_image(self.maintitle, paragraphs_text_pure, imagelink, prs)


                # ------ IF needs to be used layout picture slide -----------#
                elif self.Mainimage_array:
                    logging.debug(abcd[g])
                    self.image_slide(paragraphs_text_pure, self.Mainimage_array[0], prs)


                # ------ IF needs to be used layout text without picture -----------#
                else:
                    logging.debug(abcd[g])
                    self.GetMoreImages(self.maintitle, paragraphs_text_pure, abcd[g], prs)

                # ------ inserting summary paragraphs ----------- #
        # ------ code for inserting summary slides ends here -----------#



        # ------------ parse wiki and create presentation ------------- #
        datasection = html.text
        pqr = re.split('<h2>|<h3>|<h4>', datasection)
        abc = b.find("div", {"id": "toc"})
        for i in range(2, len(pqr) - 4):
            # print('imagelink_check: ' + imagelink_check)

            # ------ wiki web-scraping code starts here -----------#
            deliverd = pqr[i]
            imagelink = ''
            paragraphs_text = ''
            subsection = deliverd
            c = BeautifulSoup(subsection, 'html.parser')
            subsectiontitle = c.find("span", {"class": "mw-headline"})
            subsectiontitle = subsectiontitle.text.strip()
            if subsectiontitle in sections_array_list:
                # section name
                # print(subsectiontitle)
                paragraph = c.find_all("p")
                for k in range(0, len(paragraph)):
                    logging.debug(paragraph[k])
                    paragraphs_text = paragraph[k].text.strip()
                    paragraphs_text = paragraphs_text.split('[')
                    paragraphs_text_pure = ''
                    if len(paragraphs_text) > 1:
                        for v in range(0, len(paragraphs_text)):
                            paragraphs_text[v] = paragraphs_text[v].split(']')
                            if len(paragraphs_text[v]) > 1:
                                paragraphs_text_pure = paragraphs_text_pure + paragraphs_text[v][1]
                            else:
                                paragraphs_text_pure = paragraphs_text_pure + paragraphs_text[v][0]
                    else:
                        paragraphs_text_pure = paragraphs_text[0]
                    logging.debug(imagelink)

                    if 'class="image"' in subsection:
                        abc = c.find_all("a", {"class": "image"})
                        if len(abc) >= k:
                            try:
                                # pargarph link
                                imagelink = str(abc[k].img).split('src="', 1)[1]
                                imagelink = imagelink.split('"', 1)[0]
                                imagelink_temp = imagelink.split('/')

                                imagelink_temp_size = imagelink_temp[-1].split('-')
                                del imagelink_temp[-1]
                                imagelink_temp_size[0] = self.image_get_width
                                imagelink_temp_size = '-'.join([str(x) for x in imagelink_temp_size])
                                imagelink_temp.append(imagelink_temp_size)
                                imagelink_temp = '/'.join([str(x) for x in imagelink_temp])

                                imagelink_temp2 = 'https:' + str(imagelink_temp)
                                if imagelink_temp2 != self.unexcepted_images and '.svg' not in imagelink_temp2:
                                    # print('check svg - ' + imagelink)
                                    imagelink = imagelink_temp2
                                else:
                                    imagelink = ''

                            except Exception as e:
                                pass
                                # print(e)
                    # ------ wiki web-scraping code ends here -----------#

                    # ------ IF section is the same as the slide before
                    if self.section_current_name == subsectiontitle:
                        self.page_data['sections'][-1]["paragraphs"].append({

                            # text inside <p></p>
                            "text": paragraphs_text_pure,
                            # Source text
                            "source": paragraph[k],
                            # URL of nearest to paragraph image
                            "image": imagelink

                        })

                        # ------ IF needs to be used regular layout text title slide with image -----------#
                        if len(paragraphs_text_pure) > 200 and imagelink:
                            # print('1 '+str(len(paragraphs_text_pure)))

                            if self.imagelink_check == imagelink:
                                # print('imagelink_check same as imagelink')
                                self.GetMoreImages(subsectiontitle, paragraphs_text_pure, paragraph[k], prs)


                            else:
                                imagelink_check = imagelink
                                self.regular_slide_text_title_image(subsectiontitle, paragraphs_text_pure, imagelink, prs)


                        # ------ IF needs to be used layout picture slide -----------#
                        elif imagelink:
                            self.imagelink_check = imagelink
                            self.image_slide(paragraphs_text_pure, imagelink, prs)


                        # ------ IF needs to be used layout text without picture -----------#
                        else:
                            self.GetMoreImages(subsectiontitle, paragraphs_text_pure, paragraph[k], prs)





                    # ------ IF starts a new section --------#
                    else:
                        self.section_current_name = subsectiontitle
                        self.page_data['sections'].append({
                            # section title
                            "title": subsectiontitle,
                            # array of paragraphs in section
                            "paragraphs":
                                [
                                    {
                                        # text inside <p></p>
                                        "text": paragraphs_text_pure,
                                        # Source text
                                        "source": paragraph[k],
                                        # URL of nearest to paragraph image
                                        "image": imagelink
                                    },
                                ]
                        })

                        # ------ Insert hero slide for new section -----------#
                        self.hero_slide(subsectiontitle, imagelink, prs)

                        # ------ IF needs to be used regular layout text title slide -----------#
                        if len(paragraphs_text_pure) > 200 and imagelink:
                            # print('4 '+str(len(paragraphs_text_pure)))
                            self.imagelink_check = imagelink

                            self.regular_slide_text_title_image(subsectiontitle, paragraphs_text_pure, imagelink, prs)

                        # ------ IF needs to be used layout picture slide -----------#
                        elif imagelink:
                            self.imagelink_check = imagelink
                            self.image_slide(paragraphs_text_pure, imagelink, prs)

                        # ------ IF needs to be used layout text without picture -----------#
                        else:
                            self.GetMoreImages(subsectiontitle, paragraphs_text_pure, paragraph[k], prs)



        Mainimage_array = []
        # ------------------ /// --------------------#
        # ------------------ Saving file --------------------#
        presentation_stream = BytesIO()
        prs.save(presentation_stream)
        # clear_fields()

        return presentation_stream