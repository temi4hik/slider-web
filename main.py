#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from webapp2 import Route
from webapp2_extras.routes import PathPrefixRoute, DomainRoute

from handlers.api.auth import SignInApiHandler, SignUpApiHandler, LogoutApiHandler, UserInfoApiHandler, \
    UpdateUserInfoApiHandler
from handlers.api.presentation import MakePresentationHandler, \
    PresentationFileHandler, PresentationInfoHandler, PresentationImageHandler, \
    UserPresentationsHandler, DeletePresentationHandler, ConfirmPresentationHandler
from handlers.api.restore import ForgotPasswordApiHandler
from handlers.api.template import TemplatesListHandler, TemplateImageHandler, BasicTemplateImageHandler
from handlers.api.wiki import WikiSearchHandler, WikiMakePresentationHandler, WikiSectionsHandler
from handlers.tasks.presentation import DeletePresentationTaskHandler
from handlers.tasks.restore import DeleteForgotPasswordTokenHandler
from handlers.web.admin.fonts import FontListHandler, FontAddHandler
from handlers.web.admin.main import MainAdminHandler
from handlers.web.admin.templates import TemplateAddHandler, TemplateEditHandler, TemplateEditSlides
from handlers.web.admin.templates import TemplatesListHandler as TemplatesAdminListHandler
from handlers.web.auth import LogoutWebHandler, SignUpHandler, VerificationHandler, SignInHandler, DashboardHandler
from handlers.web.homepage import HomePageHandler
from handlers.web.presentation import *
from handlers.web.restore import RestorePasswordHandler, ForgotPasswordHandler
from requests_toolbelt.adapters import appengine
appengine.monkeypatch()

config = {
    'webapp2_extras.auth': {
        'user_model': 'models.user.User',
        'user_attributes': ['name']
    },
    'webapp2_extras.sessions': {
        'secret_key': 'YOUR_SECRET_KEY'
    }
}

# noinspection PyUnresolvedReferences
app = webapp2.WSGIApplication([
    Route('/', HomePageHandler, 'main'),
    Route('/sign_up', SignUpHandler, 'sign_up'),
    Route('/dashboard', DashboardHandler, 'dashboard'),
    Route('/sign_in', SignInHandler, 'sign_in'),
    Route('/verification', VerificationHandler, 'verification'),
    Route('/forgot', ForgotPasswordHandler, 'forgot_password'),
    Route('/restore', RestorePasswordHandler, 'restore_password'),
    Route('/logout', LogoutWebHandler, 'logout_web_handler'),

    PathPrefixRoute('/dashboard', [
        Route('/', DashboardHandler),
        Route('/my_templates', DashMyTemplatesHandler, 'dash_my_templates'),
    ]),

    PathPrefixRoute('/presentation', [
        Route('/', PresentationPageHandler, 'presentation_main'),
        Route('/create', CreatePresentationWindowHandler, 'presentation_create'),
        Route('/content', PresentationContentWindowHandler, 'presentation_content'),
        Route('/templates', TemplatesListWindowHandler, 'presentation_templates'),
        Route('/generation', GenerationWindowHandler, 'presentation_generation'),
        Route('/dash_new_pres', PresentationNewWebHandler, 'presentation_new_web'),
        Route('/my_presentations', MyPresentationsWebHandler, 'my_presentations'),
        Route('/save', SaveWindowHandler, 'presentation_save'),
        Route('/get', GetPresentationHandler, 'presentation_get'),

    ]),

    PathPrefixRoute('/api', [
        PathPrefixRoute('/wiki', [
            Route('/search', WikiSearchHandler),
            Route('/sections', WikiSectionsHandler),
            Route('/make', WikiMakePresentationHandler),
        ]),
        PathPrefixRoute('/presentation', [
            Route('/make', MakePresentationHandler),
            PathPrefixRoute(r'/<presentation_id>', [
                Route('/info', PresentationInfoHandler),
                Route('/file', PresentationFileHandler, 'presentation_file'),
                Route('/image', PresentationImageHandler, 'presentation_image'),
                Route('/delete', DeletePresentationHandler, 'presentation_delete'),
                Route('/confirm', ConfirmPresentationHandler, 'presentation_confirm'),
            ])
        ]),
        PathPrefixRoute('/templates', [
            Route('', TemplatesListHandler),
            PathPrefixRoute(r'/<template_id>', [
                Route('/image', TemplateImageHandler, 'template_image')
            ])
        ]),
        PathPrefixRoute('/basic_template', [
            PathPrefixRoute(r'/<template_id>', [
                Route('/image', BasicTemplateImageHandler, 'basic_template_image'),
                # Route('/file', BasicTemplateFileHandler, 'basic_template_file'),
            ])
        ]),
        PathPrefixRoute('/auth', [
            Route('/sign_in', SignInApiHandler, 'sign_in_api'),
            Route('/sign_up', SignUpApiHandler, 'sign_up_api'),
            Route('/log_out', LogoutApiHandler, 'log_out_api'),
            Route('/forgot', ForgotPasswordApiHandler, 'forgot_password_api'),
        ]),
        PathPrefixRoute('/user', [
            Route('/info', UserInfoApiHandler, 'get_user_info'),
            Route('/update', UpdateUserInfoApiHandler, 'update_user_info'),
            Route('/presentations', UserPresentationsHandler, 'user_presentations')
        ]),
    ]),
    PathPrefixRoute('/admin', [
        Route('/', MainAdminHandler, 'admin_main'),
        PathPrefixRoute('/templates', [
            Route('/add', TemplateAddHandler, 'add_template'),
            Route('/add_in_dashboard', TemplateAddDashboardHandler, 'add_in_dashboard'),
            Route('/edit', TemplateEditHandler, 'edit_template'),
            Route('/slides', TemplateEditSlides, 'edit_slides'),
            Route('', TemplatesAdminListHandler, 'templates_list')
        ]),
        PathPrefixRoute('/fonts', [
            Route('/add', FontAddHandler, 'add_font'),
            Route('', FontListHandler, 'fonts_list')
        ])
    ]),
    PathPrefixRoute('/task', [
        Route('/delete_presentation', DeletePresentationTaskHandler, 'delete_presentation_task'),
        Route('/delete_password_token', DeleteForgotPasswordTokenHandler, 'delete_password_token_task')
    ])

], debug=True, config=config)
